<?php

return [
    /**
     * Append this item to menu sidebar
     */
    'menu' => [
        'user' => [
            'label' => 'Người dùng',
            'icon' => 'fa fa-user',
            'url' => 'user',
            'type' => 'dropdown',
            'permission' => '',
            'priority' => 10,
            'group' => 'main.management',
            'active' => 'user/*/*',
            'child' => [
                'user' => [
                    'label' => 'Quản lý người dùng',
                    'url' => '/user/manage',
                    'permission' => ''
                ],
                'role' => [
                    'label' => 'Quản lý quyền hạn',
                    'url' => '/user/roles',
                    'permission' => ''
                ],
            ],
        ],
    ],

    /**
     * List of permission. Etc: 'user: create something'
     */
    'permission' => [
        'user' => [
            'label' => 'Người dùng',
            'icon' => '',
            'permissions' => [
                'user: access' => 'Truy cập khu vực quản lý tài khoản',
                'user: create' => 'Tạo mới tài khoản',
                'user: edit' => 'Sửa tài khoản',
                'user: update status' => 'Thay đổi trạng thái tài khoản',
                'user: manage permission' => 'Phân quyền tài khoản',
            ],
        ],
    ],
];
