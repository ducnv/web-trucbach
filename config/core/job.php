<?php

return [
    /**
     * Append this item to menu sidebar
     */
    'menu' => [
        'job' => [
            'label' => 'Việc làm',
            'icon' => 'fa fa-cogs',
            'url' => 'job',
            'type' => 'dropdown',
            'permission' => '',
            'priority' => 100,
            'group' => 'main.management',
            'active' => 'job/*/*',
            'child' => [
                'job' => [
                    'label' => 'Danh sách việc làm',
                    'url' => '/job/manage',
                    'permission' => ''
                ],
                'company' => [
                    'label' => 'Danh sách công ty',
                    'url' => '/job/company',
                    'permission' => ''
                ],
                'industrialzone' => [
                    'label' => 'Khu công nghiệp',
                    'url' => '/job/industrialzone',
                    'permission' => ''
                ],
                'carrer' => [
                    'label' => 'Ngành nghề',
                    'url' => '/job/carrer',
                    'permission' => ''
                ],
                'salary' => [
                    'label' => 'Mức lương',
                    'url' => '/job/salary',
                    'permission' => ''
                ],
                'level' => [
                    'label' => 'Trình độ',
                    'url' => '/job/level',
                    'permission' => ''
                ],
                'experience' => [
                    'label' => 'Kinh nghiệm',
                    'url' => '/job/experience',
                    'permission' => ''
                ],
                'position' => [
                    'label' => 'Cấp bậc/Vị trí',
                    'url' => '/job/position',
                    'permission' => ''
                ]

            ],
        ],
    ],

    /**
     * List of permission. Etc: 'user: create something'
     */
    'permission' => [
        'job' => [
            'label' => 'Quản lý Việc Làm',
            'icon' => '',
            'permissions' => [

            ],
        ],
    ],
];
