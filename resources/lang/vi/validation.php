<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Trường following language lines contain Trường default error messages used by
    | Trường validator class. Some of Trườngse rules have multiple versions such
    | as Trường size rules. Feel free to tweak each of Trườngse messages here.
    |
    */

    'accepted'             => 'Trường :attribute phải là accepted.',
    'active_url'           => 'Trường :attribute không phải là URL hợp lệ.',
    'after'                => 'Trường :attribute phải là ngày sau :date.',
    'after_or_equal'       => 'Trường :attribute phải là xa hơn hoặc bằng :date.',
    'alpha'                => 'Trường :attribute chỉ bao gồm chữ cái.',
    'alpha_dash'           => 'Trường :attribute có thể bao gồm chữ cái, chữ số, gạch chân.',
    'alpha_num'            => 'Trường :attribute có thể bao gồm chữ cái và chữ số.',
    'array'                => 'Trường :attribute phải là một mảng.',
    'before'               => 'Trường :attribute phải là ngày trước :date.',
    'before_or_equal'      => 'Trường :attribute phải là ngày gần hơn hoặc bằng :date.',
    'between'              => [
        'numeric' => 'Trường :attribute phải là nằm giữa :min và :max.',
        'file'    => 'Trường :attribute phải là nằm giữa :min và :max kilobytes.',
        'string'  => 'Trường :attribute phải là nằm giữa :min và :max characters.',
        'array'   => 'Trường :attribute must have nằm giữa :min và :max items.',
    ],
    'boolean'              => 'Trường :attribute field phải là true hoặc false.',
    'confirmed'            => 'Trường :attribute xác thực không chính xác.',
    'date'                 => 'Trường :attribute không phải kiểu ngày tháng.',
    'date_format'          => 'Trường :attribute does not match Trường format :format.',
    'different'            => 'Trường :attribute và :other phải là different.',
    'digits'               => 'Trường :attribute phải là :digits digits.',
    'digits_between'       => 'Trường :attribute phải là between :min và :max digits.',
    'dimensions'           => 'Trường :attribute has invalid image dimensions.',
    'distinct'             => 'Trường :attribute field has a duplicate value.',
    'email'                => 'Trường :attribute phải là a valid email address.',
    'exists'               => 'Trường selected :attribute is invalid.',
    'file'                 => 'Trường :attribute phải là a file.',
    'filled'               => 'Trường :attribute field must have a value.',
    'image'                => 'Trường :attribute phải là an image.',
    'in'                   => 'Trường selected :attribute is invalid.',
    'in_array'             => 'Trường :attribute field does not exist in :other.',
    'integer'              => 'Trường :attribute phải là an integer.',
    'ip'                   => 'Trường :attribute phải là a valid IP address.',
    'ipv4'                 => 'Trường :attribute phải là a valid IPv4 address.',
    'ipv6'                 => 'Trường :attribute phải là a valid IPv6 address.',
    'json'                 => 'Trường :attribute phải là a valid JSON string.',
    'max'                  => [
        'numeric' => 'Trường :attribute may not be greater than :max.',
        'file'    => 'Trường :attribute may not be greater than :max kilobytes.',
        'string'  => 'Trường :attribute may not be greater than :max characters.',
        'array'   => 'Trường :attribute may not have more than :max items.',
    ],
    'mimes'                => 'Trường :attribute phải là a file of type: :values.',
    'mimetypes'            => 'Trường :attribute phải là a file of type: :values.',
    'min'                  => [
        'numeric' => 'Trường :attribute phải tối thiếu :min.',
        'file'    => 'Trường :attribute phải tối thiếu :min kilobytes.',
        'string'  => 'Trường :attribute phải tối thiếu :min ký tự.',
        'array'   => 'Trường :attribute phải tối thiếu :min item.',
    ],
    'not_in'               => 'Trường selected :attribute is invalid.',
    'numeric'              => 'Trường :attribute phải là a number.',
    'present'              => 'Trường :attribute field phải là present.',
    'regex'                => 'Trường :attribute format is invalid.',
    'required'             => 'Trường :attribute field is required.',
    'required_if'          => 'Trường :attribute field is required when :other is :value.',
    'required_unless'      => 'Trường :attribute field is required unless :other is in :values.',
    'required_with'        => 'Trường :attribute field is required when :values is present.',
    'required_with_all'    => 'Trường :attribute field is required when :values is present.',
    'required_without'     => 'Trường :attribute field is required when :values is not present.',
    'required_without_all' => 'Trường :attribute field is required when none of :values are present.',
    'same'                 => 'Trường :attribute và :other phải giống nhau.',
    'size'                 => [
        'numeric' => 'Trường :attribute phải là :size.',
        'file'    => 'Trường :attribute phải là :size kilobytes.',
        'string'  => 'Trường :attribute phải là :size characters.',
        'array'   => 'Trường :attribute must contain :size items.',
    ],
    'string'               => 'Trường :attribute phải là string.',
    'timezone'             => 'Trường :attribute phải là timezone.',
    'unique'               => 'Trường :attribute đã tồn tại. Vui lòng sử dụng thông tin khác.',
    'uploaded'             => 'Trường :attribute failed to upload.',
    'url'                  => 'Trường :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using Trường
    | convention "attribute.rule" to name Trường lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | Trường following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
