/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");
window.Vue = require("vue");
import vSelect from "vue-select";

// Register third-party component
Vue.component("v-select", vSelect);
require("./lib/vue-notify.js");

// ======= Manage User ======= //

Vue.component("manage-user", require("./components/User/ManageUsers.vue"));
Vue.component("manage-permission-user", require("./components/User/ManagePermission"));


// ======= End ======= //

// Carrer //
Vue.component("carrer", require("./components/Carrer/Manage"));

Vue.component("salary", require("./components/Salary/Manage"));

Vue.component("level", require("./components/Level/Manage"));

Vue.component("experience", require("./components/Experience/Manage"));

Vue.component("position", require("./components/Position/Manage"));

Vue.component("industrialzone", require("./components/IndustrialZone/Manage"));

Vue.component("company", require("./components/Company/Manage"));

Vue.component("jobpost", require("./components/JobPost/Manage"));

Vue.component("create-jobpost", require("./components/JobPost/Create"));

Vue.component("edit-jobpost", require("./components/JobPost/Edit"));

Vue.component("manage-jobpost", require("./components/JobPost/Manage"));

Vue.component("manage-media", require("./components/Media/ManageMedia"));

Vue.component("category", require("./components/Category/Manage"));

Vue.component("create-news", require("./components/News/Create"));

// ======= Manage Crawler ======= //

// Vue.component("manage-crawl", require("./components/Crawl/ManageCrawl.vue"));
// Vue.component("site-crawler", require("./components/Crawl/SiteCrawler.vue"));
// Vue.component("category", require("./components/Crawl/Category.vue"));
//
// Vue.component("edit-company", require("./components/Crawl/EditCompany.vue"));
// Vue.component("company", require("./components/Crawl/Company.vue"));
// Vue.component("edit-job", require("./components/Crawl/EditJob.vue"));
Vue.component("vue-tinymce", require("./components/Crawl/TinyMCE.vue"));
//
// Vue.component("site-client", require("./components/Crawl/SiteClient.vue"));
// Vue.component("add-category-siteclinet", require("./components/Crawl/AddCategorySiteClient.vue"));

// ======= End ======= //


import datePicker from 'vue-bootstrap-datetimepicker';
import 'bootstrap/dist/css/bootstrap.css';
import 'eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css';

Vue.use(datePicker);

Vue.component('range-datepickers', require('./components/DateRange.vue'));

// Register chart
import Chartkick from 'chartkick'
import VueChartkick from 'vue-chartkick'
import Chart from 'chart.js'
import VModal from 'vue-js-modal'

Vue.use(VModal)
Vue.use(VueChartkick, {Chartkick})

const app = new Vue({
    el: "#app"
});
