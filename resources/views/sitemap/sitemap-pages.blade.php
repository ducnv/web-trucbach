<?php echo '<' . '?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"
        xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>{{url('/')}}/gioi-thieu.html</loc>
        <lastmod>2018-09-27</lastmod>
        <changefreq>always</changefreq>
        <priority>1.0</priority>
    </url>
    <url>
        <loc>{{url('/')}}/du-hoc.html</loc>
        <lastmod>2018-09-27</lastmod>
        <changefreq>always</changefreq>
        <priority>1.0</priority>
    </url>
    <url>
        <loc>{{url('/')}}/xuat-khau-lao-dong.html</loc>
        <lastmod>2018-09-27</lastmod>
        <changefreq>always</changefreq>
        <priority>1.0</priority>
    </url>
    <url>
        <loc>{{url('/')}}/dao-tao.html</loc>
        <lastmod>2018-09-27</lastmod>
        <changefreq>always</changefreq>
        <priority>1.0</priority>
    </url>
    <url>
        <loc>{{url('/')}}/dao-hang.html</loc>
        <lastmod>2018-09-27</lastmod>
        <changefreq>always</changefreq>
        <priority>1.0</priority>
    </url>
    <url>
        <loc>{{url('/')}}/tin-tuc.html</loc>
        <lastmod>2018-09-27</lastmod>
        <changefreq>always</changefreq>
        <priority>1.0</priority>
    </url>
    <url>
        <loc>{{url('/')}}/lien-he.html</loc>
        <lastmod>2018-09-27</lastmod>
        <changefreq>always</changefreq>
        <priority>1.0</priority>
    </url>
</urlset><!-- Request ID: cfffdead328ba6a8ab5a23fae9d84405; Queries for sitemap: 7; Total queries: 24; Seconds: 0.1; Memory for sitemap: 2MB; Total memory: 10.75MB -->