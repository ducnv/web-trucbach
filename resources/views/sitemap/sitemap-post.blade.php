<?php echo '<' . '?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"
        xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach($news as $job)
        <url>
            <loc>
                {{url('/').'/'.$job->category->slug . "/" . $job->slug . ".html"}}
            </loc>
            <lastmod>{{$job->updated_at->toW3cString()}}</lastmod>
            <changefreq>always</changefreq>
            <priority>0.6</priority>
        </url>
    @endforeach
</urlset>