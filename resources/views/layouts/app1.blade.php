<!DOCTYPE html>
<html>
<head>
    <title>@yield('title','Xuất khẩu lao động Nhật Bản - Việc Làm Nhật Bản')</title>
    <meta name="description"
          content="@yield('description', "Xuất khẩu lao động Nhật Bản - Việc Làm Nhật Bản")"/>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="REFRESH" content="300"/>
    <meta name="keywords"
          content="@yield('keywords',"Xuất khẩu lao động, Xuất khẩu lao động nhật bản, Du học nhật bản, Kỹ sư nhật bản, Du học nhật vừa học vừa làm")">
    <meta property="fb:pages" content=""/>
    <meta property="og:author" content=""/>
    <meta property="og:locale" content="vi_VN"/>
    <meta property="og:site_name" content="Việc Làm Nhật Bản"/>
    <meta property="og:title"
          content="@yield('og_title', "Việc Làm Nhật Bản")"/>
    <meta property="og:url" content="{{URL::current()}}"/>
    <meta property="og:type" content="@yield('og_type',"website")"/>
    <link rel="canonical" href="{{URL::current()}}"/>
    <link rel='shortlink' href=''/>
    <meta name="robots" content="index,follow,noodp,noydir"/>
    <link rel="alternate" href="{{URL::current()}}" hreflang="vi_VN"/>
    <meta property="og:description"
          content="@yield('og_description',"Việc Làm Nhật Bản")"/>
    <meta property="og:image" content="@yield('image',"/images/banner1.jpg")"/>
    <meta property="article:publisher" content=""/>
    <meta property="fb:app_id" content=""/>
    <link href="/images/favicon.ico" rel="shortcut icon" type="image/x-icon"/>

    <link rel="stylesheet" href="/node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/node_modules/owl.carousel/dist/assets/owl.carousel.min.css">
    <link href="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/css/gijgo.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="/node_modules/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto|Oswald">
    <link rel="stylesheet" href="/css/style.css?v=1.1.1">
    <link rel="stylesheet" href="/css/css.css?v=1.0.4">
    <link rel="stylesheet" href="/css/responsive.css?v=1.0.4">
    <meta name="google-site-verification" content="xOupqxw5QOBlO_1vTU2sWpAE6Y802blLBYafHu5vwyA" />
    <script src="/node_modules/jquery/dist/jquery.min.js"></script>

    @yield("header")

</head>
<body>
@yield("menu")
<main class="main">
    @yield("content")
</main>
@include('_partials.footer')

@yield('scripts')

@yield("footer")
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
<script src="/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/js/js/owl.carousel.min.js"></script>
<script src="https://cdn.rawgit.com/atatanasov/gijgo/master/dist/combined/js/gijgo.min.js"
        type="text/javascript"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.18/jquery.touchSwipe.min.js"></script>
<script src="/js/js/jquery.min.js"></script>
<script src="/js/js/bootstrap.min.js"></script>
<script src="/js/js/wow.min.js"></script>
<script src="/js/js/jquery.stellar.min.js"></script>
<script src="/js/js/rev-slider/jquery.themepunch.tools.min.js"></script>
<script src="/js/js/rev-slider/jquery.themepunch.revolution.min.js"></script>
<script src="/js/js/rev-slider/revolution.extension.actions.min.js"></script>
<script src="/js/js/rev-slider/revolution.extension.carousel.min.js"></script>
<script src="/js/js/rev-slider/revolution.extension.kenburn.min.js"></script>
<script src="/js/js/rev-slider/revolution.extension.layeranimation.min.js"></script>
<script src="/js/js/rev-slider/revolution.extension.migration.min.js"></script>
<script src="/js/js/rev-slider/revolution.extension.navigation.min.js"></script>
<script src="/js/js/rev-slider/revolution.extension.parallax.min.js"></script>
<script src="/js/js/rev-slider/revolution.extension.slideanims.min.js"></script>
<script src="/js/js/rev-slider/revolution.extension.video.min.js"></script>
<script src="/js/js/interface.js"></script>
</body>
</html>




