<!DOCTYPE HTML>
<html lang="vi">
<head>
    <title>@yield('title','Công ty TNHH Đầu tư và Phát triển Công nghệ Trúc Bạch')</title>
    <meta name="description"
          content="@yield('description', "Công ty TNHH Đầu tư và Phát triển Công nghệ Trúc Bạch")"/>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="REFRESH" content="300"/>
    <meta name="keywords"
          content="@yield('keywords',"Công ty TNHH Đầu tư và Phát triển Công nghệ Trúc Bạch")">
    <meta property="fb:pages" content=""/>
    <meta property="og:author" content=""/>
    <meta property="og:locale" content="vi_VN"/>
    <meta property="og:site_name" content="Công ty TNHH Đầu tư và Phát triển Công nghệ Trúc Bạch"/>
    <meta property="og:title"
          content="@yield('og_title', "Công ty TNHH Đầu tư và Phát triển Công nghệ Trúc Bạch")"/>
    <meta property="og:url" content="{{URL::current()}}"/>
    <meta property="og:type" content="@yield('og_type',"website")"/>
    <link rel="canonical" href="{{URL::current()}}"/>
    <link rel='shortlink' href=''/>
    <meta name="robots" content="index,follow,noodp,noydir"/>
    <link rel="alternate" href="{{URL::current()}}" hreflang="vi_VN"/>
    <meta property="og:description"
          content="@yield('og_description',"Công ty TNHH Đầu tư và Phát triển Công nghệ Trúc Bạch")"/>
    <meta property="og:image" content="@yield('image',"")"/>
    <meta property="article:publisher" content=""/>
    <meta property="fb:app_id" content=""/>


    <link rel="stylesheet" type="text/css" href="/common/v6/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="/common/v6/css/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="/common/v6/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css" href="/common/v6/css/reset.css">
    <link rel="stylesheet" type="text/css" href="/common/v6/css/custom.css?ver=6.3">
    <link rel="amphtml" href="http://amp.vnmedia.vn/index.php?url=YjgwYmI1MDljMzQzLWFtcC12bm1lZGlhLnZuLWFtcC0v"/>
    <link rel="shortcut icon" href="/common/v6/image/favicon.ico" type="icon/iconmedia.ico"/>
    <!-- link href="/common/bootstrap/css/bootstrap.min.css" rel="stylesheet" -->
    <script type="text/javascript" src="/common/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="/common/jquery/jquery.lazyload.min.js"></script>
    <script type="text/javascript" src="/common/v1/js/cate.js"></script>

    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <!-- Carousel -->
    <link rel="stylesheet" type="text/css" href="/common/v6/css/owl.carousel.css"/>
    <!-- UI css -->
    <link rel="stylesheet" type="text/css" href="/common/v6/css/jquery-ui.css"/>
    <!-- Scrollbar -->
    <link rel="stylesheet" type="text/css" href="/common/v6/css/jquery.mCustomScrollbar.css"/>
    <!-- reset css -->
    <link rel="stylesheet" type="text/css" href="/common/v6/css/reset.css"/>
    <!-- Custom Styles -->
    <link rel="stylesheet" type="text/css" href="/common/v6/css/custom.css"/>

    <script type="text/javascript" src="/common/v6/jquery/owl.carousel.min.js"></script>
    <script type="text/javascript" src="/common/v6/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/common/v6/jquery/jquery.mCustomScrollbar.js"></script>
    <script type="text/javascript" src="/common/player/jwplayer.min.js"></script>
    <script type="text/javascript" src="/common/player/player.js"></script>
    <script type="text/javascript" src="/common/v6/js/custom.js?d=20170414"></script>
    <script type="text/javascript" src="/common/v6/js/init.js"></script>
    <link rel="stylesheet" href="/sweetalert/sweetalert.css">

    @yield("header")


</head>
<body>
<div id="page">
    @include('_partials.header')

    @yield("content")


    @include('_partials.footer')
    <script type="text/javascript" src="/common/v6/js/vnmedia.js?_dc=20182807"></script>
    <script type="text/javascript" src="/common/jquery/head.min.js"></script>
    <script src="/sweetalert/sweetalert.min.js"></script>
    <script>
        $("#clickopenmodal").click(function () {
            swal({
                title: '',
                text: "<p>Đăng ký dịch vụ: DK gửi 9092 (3.000đ/ngày, gia hạn hàng ngày)</p><br><p>Chi tiết liên hệ: 0944476193 (Cước di động)</p>",
                showConfirmButton: false,
                html: true,
                allowOutsideClick: true,
            })
        });
    </script>
    @yield('scripts')

    @yield("footer")
</div>
</body>
</html>
