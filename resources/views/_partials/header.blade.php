<div class="header">
    {{--<div class="header_logo">--}}
    {{--<div class="w_inner box_logo">--}}
    {{--<a href="/">--}}
    {{--<img class="logo" src="/common/v6/image/home/logo.png" alt="Trang chu&#777; VnMedia">--}}
    {{--<!--         <img class="logo" src="http://vnmedia.vn/dataimages/201801/original/images2078063_VnMedia_logo_01.png" alt="Trang chu&#777; VnMedia" style=" margin-top:  0px !important;">  -->--}}
    {{--</a>--}}
    {{--<div style="float:right">--}}

    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    <div class="header_main">
        <link rel="stylesheet" type="text/css"
              href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        <style>
            a, abbr, acronym, address, applet, article, aside, audio, b, blockquote, big, body, center, canvas, caption, cite, code, command, datalist, dd, del, details, dfn, dl, div, dt, em, embed, fieldset, figcaption, figure, font, footer, form, h1, h2, h3, h4, h5, h6, header, hgroup, html, i, iframe, img, ins, kbd, keygen, label, legend, li, meter, nav, object, ol, output, p, pre, progress, q, s, samp, section, small, span, source, strike, strong, sub, sup, table, tbody, tfoot, thead, th, tr, tdvideo, tt, u, ul, var {
                vertical-align: middle;
            }

            .block_share {
                float: right;
            }

            .block_share a.btn_facebook {
                background: #3067a3;
                color: #fff;
            }

            .block_share a.btn_twitter {
                background: #00aced;
                color: #fff;
            }

            .block_share a.btn_google {
                background: #dd4b39;
                color: #fff;
            }

            .block_share a {
                width: 18px;
                height: 16px;
                text-align: center;
                line-height: 17px;

            }

            .block_share a {
                display: inline-block;
                margin: 0 0 0 5px;
            }
        </style>
        @include('_partials.menu')
        <style>
            .sub_nav > li > a.hot_news, .sub_nav > li > a.date_time, .sub_nav > li > a.hot_line {
                cursor: text;
            }

            .sub_nav li a {
                font-size: 12px;
                color: #6f6f6f;
                padding: 0 15px;
                line-height: 16px;
                position: relative;
            }

            .sub_nav .li_marquee {
                max-width: 365px;
            }

            #home-slider {
                background: url(http://sanlocvang.com.vn/images/background.png) 0 100% repeat-x;
                position: relative;
                margin-top: 30px;
            }
        </style>
        {{--@include('_partials.hot')--}}
    </div>
</div>

<section id="home-slider">

    @if(isset($msisdn) && $msisdn)
        <h3 style="text-align: center;padding: 30px;font-size: 24px"> Xin
            chào: {{$msisdn}}. Mời bạn
            click <a style="color: red" target="_blank" href="{{$urlRg}}">Vào
                đây</a> để đăng kí "Giữ lộc liền tay, ring ngay thưởng lớn"
        </h3>
    @else
        <h3 style="text-align: center;padding: 30px;font-size: 24px"> Xin
            chào: bạn. Mời bạn
            click <a id="clickopenmodal" style="color: red" href="javascript:void(0);">Vào
                đây</a> để đăng kí "Giữ lộc liền tay, ring ngay thưởng lớn"
        </h3>@endif


</section>
