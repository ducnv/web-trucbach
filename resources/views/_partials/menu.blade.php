<div class="box_menu_nav">
    <div class="w_inner">
        <ul class="menu_nav clearfix">
            <li><a href="/"><i class="icon-home"></i></a></li>
            <li><a href="/the-thao">Thể thao</a></li>
            <li><a href="/cong-nghe">Công nghệ</a></li>
            <li><a href="/van-hoa">Văn hoá</a></li>
            <li class="dropdown"><a class="dropbtn" href="#">Dịch vụ</a>
                <ul class="dropdown-content">
                    <li><a href="/luat-choi">Luật chơi</a></li>
                    <li><a href="/huong-dan">Hướng dẫn</a></li>
                    <li><a href="/hoi-dap">Hỏi đáp</a></li>
                    <li><a href="/giai-thuong">Giải thưởng</a></li>
                </ul>
            </li>
            <li><a href="/chinh-sach-bao-ve-thong-tin-khach-hang">Chính sách dịch vụ</a></li>
        </ul>

        <button type="button" class="navbar_mobile">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
</div>
