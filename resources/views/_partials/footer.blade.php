<div class="footer">
    <div class="w_inner clearfix">
        <div class="footer-content">
             <span>
                <h3 itemprop="name">Giấy phép ICP số 5056/GP-TTĐT, được Sở Thông tin và Truyền thông Hà Nội cấp ngày 16/10/2019</h3></span>
            <span itemprop="publisher" itemscope="1" itemtype="http://schema.org/Organization">
                <h3 itemprop="name">Công ty TNHH Đầu tư và Phát triển Công nghệ Trúc Bạch</h3></span>
            <div class="footer-content-left" itemprop="location" itemscope="1"
                 itemtype="http://schema.org/PostalAddress">
                <p>Địa chỉ: Số 185 Chùa Láng, phường Láng Thượng, quận Đống Đa, TP. Hà Nội
                </p>
                <p>Số điện thoại: 0888182238</p>
                <p>Email công ty: info@vienthongtrucbach.vn</p>
                <p>Người chịu trách nhiệm quản lý nội dung trang tin: Nguyễn Thế Hải</p>
                <p>Số điện thoại người chịu trách nhiệm quản lý nội dung trang tin: 0888182238</p>
                <p>Email người chịu trách nhiệm quản lý nội dung trang tin: haint@vienthongtrucbach.vn</p>
            </div>
            <div class="footer-content-right">
                <div class="logo-footer">
                    <a href='http://online.gov.vn/Home/WebDetails/60441'>
                        <img style="height: 75px" alt='' title='' src='http://online.gov.vn/Content/EndUser/LogoCCDVSaleNoti/logoSaleNoti.png'/>
                    </a>
                </div>
                <ul class="social">
                    <li><a class="link_google_plug" href="#"></a></li>
                    <li><a class="link_face" href="#"></a></li>
                    <li><a class="link_twitter" href="#"></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="w_inner">
        </div>
    </div>
</div>
