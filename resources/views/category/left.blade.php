<div class="col-left">
    <ul id="submenu_nav" class="breadcrumb clearfix">
        <li class="active"><a class="dropbtn" href="/{{$category->slug}}">{{$category->name}}</a>
        </li>
    </ul>
    <h1 class="head_title">
        <a href="{{getslug($news[0])}}">
            {{$news[0]->title}}
        </a>
    </h1>
    <div class="top_new_cate">
        <div class="info_new_top">
            <div class="w_100 clearfix">
                <div class="col_left_img">
                    <div class="thumb"><a
                                href="{{getslug($news[0])}}"><img
                                    class="avatar"
                                    src="{{$news[0]->thumb}}"
                                    alt="{{$news[0]->title}}">
                        </a>
                    </div>
                </div>
                <div class="col_right_info">
                    <h3 class="title_news"><a
                                href="{{getslug($news[0])}}"> {{$news[0]->description}}
                        </a>
                    </h3>
                    <div class="date_time"><span class="date"> {{$news[0]->created_at}}</span>
                    </div>
                    <div class="title"><a class="span_title" href="/{{$category->slug}}">{{$category->name}}</a>
                    </div>
                    <ul>
                        @foreach($news->slice(1,2) as $item)
                            <li>
                                <a href="{{getslug($item)}}">{{$item->title}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <ul class="list_news_top clearfix">
            @foreach($news->slice(0,3) as $item)
                <li>
                    <div class="block_image_news">
                        <div class="thumb"><a
                                    href="{{getslug($item)}}"><img
                                        src="{{$item->thumb}}"
                                        alt="{{$item->title}}">
                            </a>
                        </div>
                        <h3 class="title_news">
                            <a href="{{getslug($item)}}">{{$item->title}}</a>
                        </h3>
                        <div class="date_time"><span class="date">{{$item->created_at}}</span><span> - </span><a
                                    href="/{{$category->slug}}" class="span_title">{{$category->name}}</a>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>

    <div class="block_col">
        <div class="block_col_right">
            <style>
                .block-news-right ul.listnews > li {
                    float: left;
                    padding: 10px 0 15px;
                    border-top: 1px solid #e4e4e4;
                }

                .block-news-right .block_image_news .news_lead em {
                    font-weight: bold;
                }
            </style>
            <div class="block-news-right" id="cate-content">
                <ul class="listnews clearfix">
                    @foreach($news as $item)
                        <li>
                            <div class="block_image_news">
                                <h3 class="title_news"><a
                                            href="{{getslug($item)}}"
                                            class="title">{{$item->title}}</a>
                                </h3>
                                <div class="thumb"><a class="media-left"
                                                      href="{{getslug($item)}}"><img
                                                class="avatar vnmedia-lazy"
                                                src="{{$item->thumb}}"
                                                alt="{{$item->title}}">
                                    </a>
                                </div>
                                <div class="news_lead">{{$item->description}}
                                </div>
                                <div class="date_time"><span class="date">{{$item->created_at}}</span>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div id="cate-content-next">
                <ul id="search-news-result" class="listnews clearfix">

                </ul>
            </div>
        </div>
        <div class="block_col_left">
            <div class="block_news_left">
                <h3 class="title_box"><span>Mới nhất</span>
                </h3>
                <ul>
                    @foreach($lastNews as $k => $item)
                        <li>
                            <div class="block_image_news">
                                <div class="thumb"><a
                                            href="{{getslug($item)}}"><img
                                                src="{{$item->thumb}}"
                                                alt="{{$item->title}}">
                                    </a>
                                </div>
                                <div class="news_lead"><a
                                            href="{{getslug($item)}}"
                                            class="title Article">{{$item->title}}</a>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="block_news_left block_read_more">
                <h3 class="title_box"><span>Đọc nhiều</span>
                </h3>
                <ul>
                    @foreach($reads as $k => $item)
                        @if($k === 0)
                            <li class="first">
                                <div class="block_image_news">
                                    <div class="thumb"><a
                                                href="{{getslug($item)}}"><img
                                                    src="{{$item->thumb}}"
                                                    alt="{{$item->title}}">
                                        </a>
                                    </div>
                                    <div class="news_lead"><a
                                                href="{{getslug($item)}}"
                                                class="title Article">{{$item->title}}</a>
                                    </div>
                                </div>
                            </li>
                        @else
                            <li>
                                <div class="news_lead"><a
                                            href="{{getslug($item)}}"
                                            class="title Article">{{$item->title}}</a>
                                </div>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>

    </div>
</div>
