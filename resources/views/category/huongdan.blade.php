@extends('layouts.app')

@section('title',  "Hướng dẫn")
@section('og_title',  "Hướng dẫn")
@section('description',  "Hướng dẫn")
@section('og_description',  "Hướng dẫn")
@section('keywords',  "Hướng dẫn")

@section('header')
    <style>
        .table-bordered thead td, .table-bordered thead th {
            border-bottom-width: 2px;
        }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #dee2e6;
        }
        .table-bordered td, .table-bordered th {
            border: 1px solid #dee2e6;
        }
    </style>
@endsection

@section('content')
    <div class="wrapper"><!-- --->
        <div class="w_inner clearfix">
            <div class="col_content">
                <div class="simditor-body" >

                    <h1><b><span style="font-size: 13pt; color: rgb(64, 64, 64);">Dịch vụ</span></b></h1>
                    <p><span style="font-size: 13pt;">&nbsp; Giới thiệu tổng quan</span></p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">-Tên dịch vụ: Săn lộc vàng.</span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">-Hình thức tham gia: Qua SMS</span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">-Đầu số của dịch vụ: 9092</span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">-Địa chỉ website dịch vụ: http://sanlocvang.com.vn</span>
                    </p>
                    <p><span style="font-size: 13pt;">&nbsp; Thể lệ tham gia dịch vụ Săn lộc vàng:</span></p>
                    <p><span style="font-size: 13pt;">&nbsp; Cách chơi:Thuê bao phải soạn tin DK gửi 9092 để đăng ký dịch vụ, và nhắn tin LOC để tham gia tranh Lộc vàng (có 10 sms miễn phí hàng ngày), từ tin nhắn thứ 11 trở đi tính phí 500đ/tin nhắn thuê bao nào có phiên giữ Thẻ cào Kim cương lâu nhất trong tuần sẽ là người chiến thắng.</span>
                    </p>
                    <p><span style="font-size: 13pt;">&nbsp; Cách xác định thuê bao chiến thắng:</span></p>
                    <p><span style="font-size: 13pt;">&nbsp; Kết thúc chu kỳ dịch vụ hằng tuần từ 8h00’00 sáng thứ 2 đến 23h59’59”, hệ thống tự động tổng hợp thời gian mà mỗi thuê bao đã giữ được Lộc vàng trong tuần để làm cơ sở tính giải thưởng tuần.</span>
                    </p>
                    <p><span style="font-size: 13pt;">&nbsp; Thuê bao chiến thắng giải tuần: là thuê bao có phiên giữ Lộc vàng lâu nhất trong tuần đó tính từ thời điểm 8h00’00” ngày đầu tiên của tuần đến 23h59’59” ngày cuối cùng của tuần (Đơn vị: giờ/phút/giây).</span>
                    </p>
                    <p><span style="font-size: 13pt;">&nbsp; Trường hợp nhiều thuê bao có cùng thời gian giữ Lộc vàng của 1 tuần lâu nhất bằng nhau thì thuê bao nào giữ Lộc vàng sớm nhất là thuê bao giành giải thưởng. Nếu có thuê bao nào khác vì lý do bất khả kháng (mất sim, không liên lạc được...) thì hệ thống dịch vụ và BTC sẽ chọn ra thuê bao có số thời gian giữ Lộc vàng của 1 tuần lâu liền kề để trao giải với số lượng là 01 giải tuần theo quy định.</span>
                    </p>
                    <p><b><i><span style="font-size: 13pt;">Lưu ý: Do luật chơi đơn giản nên để cuộc chơi trở lên công bằng cho người chơi và tránh những đội săn giải thưởng thì có những quy định bắt buộc đối với NGƯỜI THAM DỰ SĂN LỘC VÀNG như sau:</span></i></b>
                    </p>
                    <p><b><span style="font-size: 13pt;">Những thuê bao tham gia không được phép sử dụng thiết bị gửi tin nhắn tự động hoặc để tự động hóa việc nhắn tin để dành thời gian hoặc bắn tin tràn lan tới hệ thống mặc dù vẫn đang sở hữu Săn lộc vàng để cản trở sự tham gia của các thuê bao khác, hoặc bất kỳ phương thức nào khác để gửi tin nhắn, mà không phải là gửi bằng điện thoại di động (cấm việc sử dụng bất kỳ bộ điều giải GSM gắn với một máy tính hoặc điện thoại di động gắn với máy tính) Vinaphone và Công ty Trúc Bạch có toàn quyền ngừng hoặc hủy bỏ kết quả của thuê bao nào có dấu hiệu vị phạm quy định này.</span></b>
                    </p>
                    <p><span style="font-size: 13pt;">&nbsp; Các cú pháp chính của gói cước:</span></p>
                    <div class="simditor-table">
                        <table class="table table-bordered" width="100%">
                            <colgroup>
                                <col width="NaN%">
                                <col width="NaN%">
                                <col width="NaN%">
                            </colgroup>
                            <thead>
                            <tr>
                                <th><span style="font-size: 13pt; color: rgb(46, 116, 181);">1</span></th>
                                <th><span style="font-size: 13pt; color: rgb(46, 116, 181);">Cú pháp đăng ký dịch  vụ Săn lộc vàng</span>
                                </th>
                                <th><b><span style="font-size: 13pt; color: rgb(46, 116, 181);">DK</span></b></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><span style="font-size: 13pt; color: rgb(46, 116, 181);">2</span></td>
                                <td><span style="font-size: 13pt; color: rgb(46, 116, 181);">Cú pháp hủy dịch vụ  Săn lộc vàng</span>
                                </td>
                                <td><b><span style="font-size: 13pt; color: rgb(46, 116, 181);">HUY SLV</span></b></td>
                            </tr>
                            <tr>
                                <td><span style="font-size: 13pt; color: rgb(46, 116, 181);">3</span></td>
                                <td><span style="font-size: 13pt;">Kiểm tra số thời gian giữ Lộc vàng</span></td>
                                <td><span style="font-size: 13pt;">KQ</span></td>
                            </tr>
                            <tr>
                                <td><span style="font-size: 13pt; color: rgb(46, 116, 181);">4</span></td>
                                <td>
                                    <span style="font-size: 13pt;">Kiểm tra xem thuê bao có đang giữ Lộc hay không</span>
                                </td>
                                <td><b><span style="font-size: 13pt;">LV</span></b></td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="simditor-resize-handle" contenteditable="false"></div>
                    </div>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span
                                style="font-size: 13pt; color: rgb(46, 116, 181);">&nbsp;</span></p>
                    <p><span style="font-size: 13pt;">&nbsp; Thời gian, địa điểm và thủ tục trao thưởng:</span></p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><b><i><span style="font-size: 13pt;">Thời gian xác nhận thuê bao chiến thắng: </span></i></b>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">- Trong vòng 7 (bảy) ngày làm việc kể từ ngày xác định được thuê bao chiến thắng, các khiếu nại về kết quả DỊCH VỤ nếu có sẽ được giải quyết. Sau thời hạn 07 (bảy) ngày này, mọi khiếu nại về kết quả của DỊCH VỤ sẽ không được tiếp nhận và giải quyết dù với bất cứ lí do nào.</span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">- Sau 07 (bảy) ngày làm việc kể từ ngày xác định được thuê bao chiến thắng, nếu không có khiếu nại hoặc bất kỳ tranh chấp nào về số thuê bao trúng thưởng thì Công ty Trúc Bạch phối hợp cùng Vinaphone sẽ tiến hành các thủ tục xác minh thuê bao chiến thắng và tiến hành các thủ tục trao giải.</span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">- Nếu có tranh chấp, quyết định của Công ty Trúc Bạch sẽ là quyết định cuối cùng có hiệu lực dựa trên những thể lệ của DỊCH VỤ. </span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><b><i><span style="font-size: 13pt;">Thủ tục, điều lệ nhận thưởng và quy định giải thưởng:</span></i></b>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><b><span style="font-size: 13pt;">Quy định trả thưởng:</span></b>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">- Sau 07 ngày làm việc kể từ ngày xác định được thuê bao chiến thắng, Vinaphone sẽ gửi tin nhắn từ đầu số 9092 thông báo về kết quả tới thuê bao chiến thắng. </span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">- Sau đó, Công ty Trúc Bạch và Vinaphone sẽ tiến hành gọi điện thoại (3lần/ngày vào 3 thời điểm khác nhau) từ số tổng đài chuyên biệt của Trúc Bạch để xác minh thuê bao chiến thắng trong 3 ngày làm việc liên tục từ 8h00 đến17h00 hàng ngày. </span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">- Trong trường hợp không thể liên hệ với thuê bao chiến thắng đầu tiên trong thời gian 03 ngày làm việc, kết quả này sẽ bị hủy (được xác nhận bởi Công ty Trúc Bạch và Vinaphone) và người có số điểm cao kế tiếp sẽ là thuê bao chiến thắng. </span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">- Trong trường hợp xác minh được thuê bao chiến thắng nhưng thuê bao đó tham gia không hợp lệ hoặc thuê bao từ chối nhận thưởng thì giải thưởng sẽ được trao cho số thuê bao có số điểm cao kế tiếp. </span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">- Quy trình xác minh thuê bao chiến thắng kế tiếp sẽ như trên, và nếu thuê bao chiến thắng thứ hai vẫn không thể liên hệ được hoặc tham gia không hợp lệ hoặc từ chối nhận giải thưởng, kết quả sẽ bịhủy và giải thưởng sẽ được trao cho thuê bao có số điểm cao kế tiếp theo. Vàquy trình sẽ tiếp tục như trên cho đến khi xác minh được thuê bao chiến thắngđể trao thưởng. </span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">- Vào thời điểm xác minh thuê báo chiến thắng, số thuê bao chiến thắng phải đang ở trạng thái hoạt động cả 2 chiều. </span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">- Trường hợp số thuê bao trúng thưởng là:</span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">- Thuê bao trả sau: giải thưởng chỉđược trao cho chủ thuê bao đứng tên trên hợp đồng tại thời điểm xác minh vàđang ở trạng thái hoạt động cả 2 chiều. </span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">- Thuê bao trả trước: giải thưởng được trao cho chủ thuê bao đang sử dụng số điện thoại đó tại thời điểm xácminh.</span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">- Nếu số thuê bao chiến thắng là thuê bao trả sau sở hữu bởi một công ty, người sử dụng số thuê bao trên thamgia DỊCH VỤ sẽ chỉ được nhận giải thưởng khi xuất trình giấy tờ xác nhận bởiđại diện hợp pháp của công ty sở hữu số thuê bao thắng cuộc nêu rõ công ty chophép người có tên đăng ký tham gia DỊCH VỤ sử dụng số thuê bao đó để tham giaDỊCH VỤ và rằng công ty từ bỏ mọi quyền lợi đới với giải thưởng mà người có tênđăng ký tham gia DỊCH VỤ nhận được.</span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">- Các thuê bao trúng thưởng phải có nghĩa vụ đóng các khoản thuế theo quy định của Nhà nước. Công ty Trúc Bạch sẽchịu trách nhiệm thu các khoản thuế hoặc các khoản phí từ thuê bao chiến thắng("Thuế Giải thưởng"), nộp thuế hoặc các khoản thu cho các cơ quan cóliên quan trước khi trao trả các giải thưởngcho những người đoạt giải phù hợp với luật pháp Việt Nam. Các thuê bao từ chốinộp thuế sẽ không nhận được giải thưởng. </span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">- Giải thưởng của DỊCH VỤ chỉ được trao cho chủ thuê bao chiến thắng đáp ứng đầy đủ các điều kiện và thể lệ củaDỊCH VỤ. Trường hợp chủ thuê bao không thể nhận giải có thể làm giấy ủy quyềncó xác nhận của cấp có thẩm quyền (Phường, xã)trong đó ghi rõ ủy thác đi nhận giải thưởng của DỊCH VỤ cho người khác nhậnthay. </span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><b><span style="font-size: 13pt;">Cơ cấu Giải thưởng</span></b>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">01 Giải nhất dành cho thuê bao có thời gian giữ Lộc vàng lâu nhất: 01 thẻ cào Vinaphone trịgiá 500.000 Đ</span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">02 Giải nhì dành cho 02 thuê bao có thời gian giữ Lộc vàng nhiều thứ 2 và thứ 3: Mỗi giải01 thẻ cào Vinaphone trị giá 100.000 Đ</span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><b><span style="font-size: 13pt;">Địa điểm nhận giải thưởng</span></b>
                    </p>
                    <p style="margin-left: 4.3pt; text-align: justify;"><span style="font-size: 13pt;">Đối với các khách hàng trúng thưởng: Công ty Trúc bạch sẽ thuê đơn vị chuyển phát thực hiện chuyển phát quà tặng đến tay người trúng giải sau khi đã thực hiện đầy đủ các điều kiện nhận giải thưởng.</span>
                    </p>
                    <p style="margin-left: 4.3pt; text-align: justify;"><b><span style="font-size: 13pt;">Quy trình xác minh chủ thuê bao trúng thưởng:</span></b>
                    </p>
                    <p style="margin-left: 4.3pt; text-align: justify;"><span style="font-size: 13pt;">Việc thực hiện xác minh chủ nhân thuê bao trúng thưởng sẽ được thực hiện theo các bước như sau:</span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">- Bước 1: Gửi tin nhắn từ đầu số 9092 thông báo về kết quả tới thuê bao chiến thắng.</span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">- Bước 2: Công ty Trúc Bạch sẽ liên hệ điện thoại (3 lần/ngàyvào 3 thời điểm khác nhau) từ số máy chăm sóc khách hàng của Trúc Bạch để xác minh thuê bao chiến thắng trong 3 ngày làm việc liên tục từ 8h00 đến 17h00 hàngngày. </span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">- Bước 3: Nếu không liên hệ được với chủ nhân thuê bao trúngthưởng thì giải thưởng sẽ được trao cho thuê bao có số điểm cao nhiều thứ hai.Và quy trình xác minh thuê bao thứ hai này sẽ tương tự như trên.</span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">- Bước 4: Đối với thuê bao trả sau, phải thanh toán cước tháng đầy đủ thì mới được nhận thưởng.</span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">- Bước 5: Trao thưởng: Công ty Trúc Bạch tiến hành trả thưởng cho khách hàng sau khi xác minh hoàn tất và trong thời gian cam kết của DỊCHVỤ.</span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><b><i><span style="font-size: 13pt;">Những quy định khác:</span></i></b>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">- Hệ thống của Trúc Bạch sẽ kiểm soát thời gian nhận tin nhắn của người chơi.</span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">- Những thuê bao tham gia không được phép sử dụng thiết bị gửi tin nhắn tự động hoặc để tự động hóa việc nhắn tin để dành thời gian hoặc bắn tin tràn lan tới hệ thống mặc dù vẫn đang sở hữu Lộc vàng để cản trở sựtham gia của các thuê bao khác, hoặc bất kỳ phương thức nào khác để gửi tin nhắn, mà không phải là gửi bằng điện thoại di động (cấm việc sử dụng bất kỳ bộđiều giải GSM gắn với một máy tính hoặc điện thoại di động gắn với máy tính)Công ty Trúc Bạch có toàn quyền ngừng hoặc hủy bỏ kết quả của thuê bao nào có dấu hiệu vị phạm quy định này.</span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">- Ban tổ chức có quyền sử dụng tên,tuổi, địa chỉ và các hình ảnh, giọng nói của người trúng thưởng cho mục đích quảng bá truyền thông DỊCH VỤ mà không cần xin phép hay trả bất kỳ chi phí nào thêm cho người trúng thưởng.</span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">- Trong trường hợp xảy ra tranh chấp liên quan đến DỊCH VỤ cung cấp này, Công ty Trúc Bạch có trách nhiệm trực tiếp giải quyết, nếu không thỏa thuận được tranh chấp sẽ được xử lý theo quy định của pháp luật.</span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">- Sau khi kết thúc DỊCH VỤ cung cấp, công ty Trúc Bạch có trách nhiệm báo cáo kết quả thực hiện DỊCH VỤ cung cấp trên theo đúng quy định của pháp luật về thời hạn báo cáo, hồ sơ báo cáo và các giấy tờ kèm theo (biên bản xác định trúng thưởng, danh sách khách hàng trúng thưởng, biên bản trao thưởng hoặc tài liệu chứng minh giải thưởng đã được trao).</span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">- Tất cả những thuê bao tham gia DỊCH VỤ được coi là đã đọc, hiểu, đồng ý và tuân thủ các quy định và thể lệ củaDỊCH VỤ. </span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">- Bố mẹ và/hoặc người giám hộ của những thuê bao dưới 18 tuổi được coi là đã đọc, hiểu, đồng ý và tuân thủ các quy định và thể lệ của DỊCH VỤ. </span>
                    </p>
                    <p style="margin-left: 8.65pt; text-align: justify;"><span style="font-size: 13pt;">- Khách hàng có thể gọi trực tiếp đến dịch vụ chăm sóc khách hàng của Vinaphone nếu có khiếu nại hoặc thắc mắc thêm.</span>
                    </p>
                    <p><span style="font-size: 13pt;">&nbsp;</span></p>
                    <p><span style="font-size: 13pt;">&nbsp;</span></p>
                    <p><span style="font-size: 13pt;">&nbsp;</span></p>
                    <p><span style="font-size: 13pt;">&nbsp;</span></p>
                    <p><span style="font-size: 13pt;">&nbsp;</span></p>
                    <p><span style="font-size: 13pt;">&nbsp;</span></p>
                    <p><span style="font-size: 13pt;">&nbsp;</span></p>
                    <p><span style="font-size: 13pt;">&nbsp;</span></p>
                    <p><span style="font-size: 13pt;">&nbsp;</span></p>
                    <p><span style="font-size: 13pt;">&nbsp;</span></p>
                    <p><span style="font-size: 13pt;">&nbsp;</span></p>
                    <p><span style="font-size: 13pt;">&nbsp;</span></p>
                    <p><span style="font-size: 13pt;">&nbsp;</span></p>
                    <p><span style="font-size: 13pt;">&nbsp;</span></p>
                    <p><span style="font-size: 13pt;">&nbsp;</span></p>
                    <p><span style="font-size: 13pt;">&nbsp;</span></p>
                    <p><span style="font-size: 13pt;">&nbsp;</span></p>
                    <p><span style="font-size: 13pt;">&nbsp;</span></p>
                    <p><span style="font-size: 13pt;">&nbsp;</span></p>
                    <p><span style="font-size: 13pt;">&nbsp;</span></p>
                    <p><span style="font-size: 13pt;">&nbsp;</span></p>
                    <p><span style="font-size: 13pt;">&nbsp;</span></p>
                    <p><span style="font-size: 13pt;">&nbsp;</span></p>
                    <p><span style="font-size: 13pt;">&nbsp;</span></p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
@section('footer')

@endsection
