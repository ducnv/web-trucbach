@extends('layouts.app')

@section('title',  "Chính sách bảo vệ thông tin khách hàng")
@section('og_title',  "Chính sách bảo vệ thông tin khách hàng")
@section('description',  "Chính sách bảo vệ thông tin khách hàng")
@section('og_description',  "Chính sách bảo vệ thông tin khách hàng")
@section('keywords',  "Chính sách bảo vệ thông tin khách hàng")

@section('header')

@endsection

@section('content')
    <div class="wrapper"><!-- --->
        <div class="w_inner clearfix">
            <div class="col_content">
                <div class="simditor-body" ><p><b><span style="font-size: 10.5pt;">CHÍNH SÁCH DỊCH VỤ</span></b><br>
                    </p>
                    <p><b><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">I. CHÍNH SÁCH GIAO NHẬN, BẢO HÀNH, ĐỔI TRẢ VÀ HOÀN TIỀN</span></b>
                    </p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">- Khi hệ thống nhận được lệnh đăng ký hợp lệ, hệ thống sẽ ngaylập tức ghi nhận chính sách thuê bao được hưởng trên dịch vụ hoặc gửi nội dungKhách hàng đã mua về thiết bị của Khách hàng.</span>
                    </p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">- Sanlocvang.com.vn không áp dụng chính sách bảo hành, đổi trảsản phẩm khi nội dung đã được chuyển đến thiết bị của khách hàng thành công.</span>
                    </p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">- Tất cả các giao dịch đã nhận được thông báo thành công đềukhông được hoàn trả phí từ hệ thống trong mọi trường hợp.</span>
                    </p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">- Trong trường hợp giao dịch lỗi, hệ thống đã tiến hành trừ tiềnthành công trong tài khoản của khách hàng mà khách hàng không nhận được nộidung mình đã mua hoặc không sử dụng được chính sách mà thuê bao được hưởng theogói cước đã đăng ký, hệ thống sẽ gửi lại nội dung hoặc bù lại số ngày sử dụngcho khách hàng tương ứng với nội dung bị lỗi hoặc số ngày bị gián đoạn sử dụng.</span>
                    </p>
                    <p><b><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">II. QUY TRÌNH THANH TOÁN VÀ ĐẢM BẢO AN TOÀN GIAO DỊCH</span></b>
                    </p>
                    <p><b><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">1. Quy trình thanh toán</span></b>
                    </p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">- Tổng đài 9092 là tổng đài ghi nhận và thực hiện trừ tiền theocác giao dịch được Khách hàng lựa chọn.</span>
                    </p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">- Khi thực hiện lệnh “Đăng ký gói cước” hệ thống sẽ căn cứ vàocác cú pháp được nhập để tiến hành trừ tiền đối với từng giao dịch. Đối vớithuê bao trả trước, tổng đài sẽ kết nối với nhà mạng để xác định khoản tiền cầntrừ trực tiếp ngay tại thời điểm Khách hàng yêu cầu lệnh. Đối với thuê bao trảsau, hệ thống sẽ ghi nhận số tiền phải trả trên tài khoản điện thoại và đượcghi vào hóa đơn cước của tháng tương ứng.</span>
                    </p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">- Số tiền bị trừ tương ứng với số lần giao dịch và giá của từngnội dung niêm yết công khai trên website thương mại điện tử sanlocvang.com.vn.</span>
                    </p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">Khoản tiền này sau khi nhà mạng thực hiện trừ sẽ được hoàn trảlại cho sanlocvang.com.vn.</span>
                    </p>
                    <p>
                        <b><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">2. Đảm bảo an toàn giao dịch</span></b>
                    </p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">- Đảm bảo an toàn thanh toán:</span></p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">Sanlocvang.com.vn sử dụng các dịch vụ, hệ thống thanh toán tíchhợp nhiều tính năng thông minh, chế độ bảo mật cao để đảm bảo thông tin cánhân, đảm bảo an toàn thanh toán của Khách hàng. Sanlocvang.com.vn.sẽ khôngtính tiền cho đến khi giao dịch được hoàn tất.</span>
                    </p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">Việc thanh toán dựa trên cơ chế trừ tiền của mạng viễn thông nênluôn đảm bảo độ chính xác cao nhất.</span>
                    </p>
                    <p><b><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">III. CHÍNH SÁCH BẢO VỆ THÔNG TIN CÁ NHÂN KHÁCH HÀNG</span></b>
                    </p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">- Mọi sự thu thập thông tin và sử dụng thông tin của Khách hàngđều được thông qua ý kiến của chính Khách hàng đó trước khi Khách hàng tiếnhành các giao dịch cụ thể. Việc bảo vệ thông tin cá nhân Khách hàng được thựchiện theo đúng nguyên tắc sau:</span>
                    </p>
                    <p><b><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">1. Mục đích thu thập</span></b></p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">- Thực hiện các giao dịch theo các đơn đặt hàng của Khách hàng,giới thiệu về dịch vụ, tính năng mới của website sanlocvang.com.vn, chăm sócKhách hàng</span>
                    </p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">- Tính giá, cước sử dụng</span></p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">- Quản lý việc đăng tải, bình luận của khách hàng về giao dịchyêu cầu thực hiện</span>
                    </p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">- Giải quyết các thắc mắc hay khiếu nại phát sinh khi Khách hàngsử dụng dịch vụ sanlocvang.com.vn.</span>
                    </p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">- Ngăn chặn những hoạt động vi phạm pháp luật tại website sanlocvang.com.vn.</span>
                    </p>
                    <p><b><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">2. Phạm vi sử dụng thông tin:</span></b>
                    </p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">- Chỉ sử dụng các thông tin được khách hàng đăng ký hoặc cậpnhật trên website sanlocvang.com.vn, không sử dụng các thông tin do khách hàngđưa trên các phương tiện, công cụ khác.</span>
                    </p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">- Khi cần thiết, chúng tôi có thể sử dụng những thông tin này đểliên hệ trực tiếp với khách hàng để thông báo về: thông tin về khuyến mãi vàdịch vụ mới, liên lạc và giải quyết với khách hàng những trường hợp đặc biệt…</span>
                    </p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">- Không sử dụng số điện thoại của khách hàng ngoài mục đích xácnhận và liên hệ có liên quan đến dịch vụ mà khách hàng đăng ký sử dụng của côngty.</span>
                    </p>
                    <p>
                        <b><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">3. Thời gian lưu trữ: 05 năm</span></b>
                    </p>
                    <p><b><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">4. Những người, tổ chức có thể tiếp cận thông tin đã thu thậpbao gồm:</span></b>
                    </p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">- Đơn vị chủ quản website sanlocvang.com.vn.</span>
                    </p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">- Đơn vị viễn thông khi thực hiện trừ giá cước sử dụng</span>
                    </p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">- Cơ quan nhà nước có thẩm quyền khi có yêu cầu cụ thể</span>
                    </p>
                    <p><b><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">5. Địa chỉ của đơn vị thu thập và quản lý thông tin</span></b>
                    </p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">CÔNG TY TNHH ĐẦU TỪ VÀ PHÁT TRIỂN CÔNG NGHỆ TRÚC BẠCH</span>
                    </p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">Địa chỉ: Số 185 Chùa Láng, Phường Láng Thượng, Quận Đống Đa,Thành Phố Hà Nội</span>
                    </p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">Điện thoại: 0888182238</span></p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">Email: info@vienthongtrucbach.vn</span>
                    </p>
                    <p><b><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">6. Phương thức và công cụ để người tiêu dùng tiếp cận và chỉnhsửa dữ liệu cá nhân của mình trên hệ thống thương mại điện tử của đơn vị thuthập thông tin</span></b>
                    </p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">- Người dùng có thể tiếp cận thông tin cá nhân (cụ thể là sốđiện thoại của mình) bằng cách truy cập vào website dịch vụ, vào mục tra cứu,nhập số điện thoại để biết được khách hàng có đăng ký dịch vụ hay không.</span>
                    </p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">- Về phần chỉnh sử dữ liệu cá nhân: khách hàng không chính sửđược số điện thoại vì khi khách hàng đăng ký dịch vụ thì hệ thống sẽ nhận biếtsố điện thoại của khách hàng.</span>
                    </p>
                    <p><b><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">7. Cơ chế tiếp nhận và giải quyết khiếu nại của người tiêu dùngliên quan đến việc thông tin cá nhân bị sử dụng sai mục đích hoặc phạm vi đãthông báo</span></b>
                    </p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">Khi phát hiện thông tin cá nhân (cụ thể số điện thoại) củamình bị sử dụng sai mục đích hoặc phạm vi, người dùng có quyền gọiđến Tổng đài chăm sóc khách hàng: 0888182238 hoặc gửi email khiếu nạiđến&nbsp;</span><a
                                href="mailto:info@vienthongtrucbach.vn"><span style="font-size: 10.5pt;">info@vienthongtrucbach.vn</span></a><span
                                style="font-size: 10.5pt; color: rgb(51, 51, 51);">&nbsp;vớicác thông tin, chứng cứ liên quan tới việc này. Công ty cam kết sẽphản hồi ngay lập tức trong vòng 24 tiếng để cùng khách hàng thốngnhất phương án giải quyết.</span>
                    </p>
                    <p><b><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">8. Cam kết bảo mật thông tin cá nhân của Khách hàng</span></b>
                    </p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">- Thông tin cá nhân của khách hàng trên sanlocvang.com.vn được sanlocvang.com.vncam kết bảo mật tuyệt đối theo chính sách bảo vệ thông tin cá nhân của sanlocvang.com.vn.Việcthu thập và sử dụng thông tin của mỗi khách hàng chỉ được thực hiện khi có sựđồng ý của khách hàng đó trừ những trường hợp pháp luật có quy định khác;</span>
                    </p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">- Không sử dụng, không chuyển giao, cung cấp hay tiết lộ cho bênthứ 3 nào về thông tin cá nhân của Khách hàng khi không có sự cho phép đồng ýtừ khách hàng;</span>
                    </p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">- Trong trường hợp máy chủ lưu trữ thông tin bị hacker tấn côngdẫn đến mất mát dữ liệu cá nhân khách hàng, sanlocvang.com.vn sẽ có trách nhiệmthông báo vụ việc cho cơ quan chức năng điều tra xử lý kịp thời và thông báocho Khách hàng được biết;</span>
                    </p>
                    <p><span style="font-size: 10.5pt; color: rgb(51, 51, 51);">- Tuy nhiên do hạn chế về mặt kỹ thuật, không một dữ liệu nào cóthể được truyền trên đường truyền internet mà có thể được bảo mật 100%. Do vậy,sanlocvang.com.vn không thể đưa ra một cam kết chắc chắn rằng thông tin Kháchhàng cung cấp cho sanlocvang.com.vn sẽ được bảo mật một cách tuyệt đối an toàn,và sanlocvang.com.vn không thể chịu trách nhiệm trong trường hợp có sự truy cậptrái phép thông tin cá nhân của Khách hàng, như các trường hợp khách hàng tự ýchia sẻ thông tin với người khác.</span>
                    </p>
                    <p>&nbsp;</p>
                    <p><br></p></div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
@section('footer')

@endsection
