<div class="col-right">
    @foreach($categories as $cat)
        <div class="related_new_right">
            <div class="title_box_right">
                <h3 class="txt_title"><a href="/{{$cat->slug}}" target="_blank"
                                         style="color: #ec2227;">{{$cat->name}}</a>
                </h3>
            </div>
            <ul>
                @foreach($cat->article()->where('status','yes')->get()->slice(0,3) as $k => $item)
                    @if($k === 0)
                        <li>
                            <div class="block_image_news">
                                <a href="">
                                    <img src="{{$item->thumb}}" alt="{{$item->title}}">
                                    <div class="title_news">
                                        {{$item->title}}
                                    </div>
                                </a>
                            </div>
                        </li>
                    @else
                        <li>
                            <div class="title_news">
                                <a href="#">{{$item->title}}</a>
                            </div>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
    @endforeach
</div>
