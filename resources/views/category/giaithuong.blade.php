@extends('layouts.app')

@section('title',  "Giải thưởng")
@section('og_title',  "Giải thưởng")
@section('description',  "Giải thưởng")
@section('og_description',  "Giải thưởng")
@section('keywords',  "Giải thưởng")

@section('header')
    <style>
        .table-bordered thead td, .table-bordered thead th {
            border-bottom-width: 2px;
        }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #dee2e6;
        }
        .table-bordered td, .table-bordered th {
            border: 1px solid #dee2e6;
        }
    </style>
@endsection

@section('content')
    <div class="wrapper"><!-- --->
        <div class="w_inner clearfix">
            <div class="col_content">
                <div class="simditor-body" >
                    @extends('layouts.app')

                    @section('title',  "Giải thưởng")
                    @section('og_title',  "Giải thưởng")
                    @section('description',  "Giải thưởng")
                    @section('og_description',  "Giải thưởng")
                    @section('keywords',  "Giải thưởng")

                    @section('header')
                        <style>
                            .table-bordered thead td, .table-bordered thead th {
                                border-bottom-width: 2px;
                            }
                            .table thead th {
                                vertical-align: bottom;
                                border-bottom: 2px solid #dee2e6;
                            }
                            .table-bordered td, .table-bordered th {
                                border: 1px solid #dee2e6;
                            }
                        </style>
                    @endsection

                    @section('content')
                        <div class="wrapper"><!-- --->
                            <div class="w_inner clearfix">
                                <div class="col_content">
                                    <div class="simditor-body" >

                                        <p style="text-align: center;"><b><span style="font-size: 12pt;">GIẢI THƯỞNG</span></b></p>
                                        <p style="text-align: justify;"><b><span style="font-size: 12pt;">I. CƠ CẤU GIẢI THƯỞNG</span></b>
                                        </p>
                                        <p><span style="font-size: 12pt;">·</span><span style="font-size: 12pt;"> Giải thưởng cụ thể sẽ được thông báo cho người tham gia dịch vụ vào ngày đầu tiên của tuần kế tiếp với sự thống nhất của Công ty Trúc Bạch và Vinaphone.</span>
                                        </p>
                                        <p><span style="font-size: 12pt;">·</span><span style="font-size: 12pt;"> Dịch vụ sẽ có mức giải thưởng: 01 Giải nhất và02 Giải nhì hàng tuần.</span>
                                        </p>
                                        <p><span style="font-size: 12pt;">·</span><span style="font-size: 12pt;"> <b>Giải thưởng tuần:</b>&nbsp;Người chiến thắng là người có tổng thời gian thời giữ Lộc vàng lớn nhất trong suốt khoảng thời gian một tuần (07 ngày) diễn ra theo quy định của dịch vụ.</span>
                                        </p>
                                        <p><span style="font-size: 12pt;">·</span><span style="font-size: 12pt;"> </span></p>
                                        <div class="simditor-table">
                                            <table class="table table-bordered" width="100%">
                                                <colgroup>
                                                    <col width="NaN%">
                                                    <col width="NaN%">
                                                    <col width="NaN%">
                                                    <col width="NaN%">
                                                    <col width="NaN%">
                                                    <col width="NaN%">
                                                    <col width="NaN%">
                                                    <col width="NaN%">
                                                </colgroup>
                                                <thead>
                                                <tr>
                                                    <th><b><span style="font-size: 12pt;">TT</span></b></th>
                                                    <th><b><span style="font-size: 12pt;">GIAI  ĐOẠN</span></b></th>
                                                    <th><b><span style="font-size: 12pt;">CƠ  CẤU GIẢI THƯỞNG</span></b></th>
                                                    <th><b><span style="font-size: 12pt;">NỘI  DUNG GIẢI</span></b></th>
                                                    <th><b><span style="font-size: 12pt;">THỜI GIAN ÁP DỤNG</span></b></th>
                                                    <th><b><span style="font-size: 12pt;">GIÁ  TRỊ </span></b> <b><span
                                                                    style="font-size: 12pt;">(VNĐ)  </span></b><span
                                                                style="font-size: 12pt;">(*)</span></th>
                                                    <th><b><span style="font-size: 12pt;">SL</span></b></th>
                                                    <th><b><span style="font-size: 12pt;">THÀNH  TIỀN</span></b><b><span
                                                                    style="font-size: 12pt;">&nbsp;<br>  </span></b><b><span
                                                                    style="font-size: 12pt;">(VNĐ)</span></b></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td><span style="font-size: 12pt;">1</span></td>
                                                    <td rowspan="2"><span style="font-size: 12pt;">TUẦN</span></td>
                                                    <td><span style="font-size: 12pt;">Giải nhất</span></td>
                                                    <td><span style="font-size: 12pt;">Thẻ cào Vinaphone  500.000 Đ </span></td>
                                                    <td><span style="font-size: 12pt;">07 ngày</span></td>
                                                    <td><span style="font-size: 12pt;">500.000</span></td>
                                                    <td><span style="font-size: 12pt;">1</span></td>
                                                    <td><span style="font-size: 12pt;">500.000</span></td>
                                                </tr>
                                                <tr>
                                                    <td><span style="font-size: 12pt;">2</span></td>
                                                    <td><span style="font-size: 12pt;">Giải nhì</span></td>
                                                    <td><span style="font-size: 12pt;">Thẻ cào Vinaphone  100.000 Đ </span></td>
                                                    <td><span style="font-size: 12pt;">07 ngày</span></td>
                                                    <td><span style="font-size: 12pt;">100.000</span></td>
                                                    <td><span style="font-size: 12pt;">2</span></td>
                                                    <td><span style="font-size: 12pt;">200.000</span></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" rowspan="2"><b><span style="font-size: 12pt;">TỔNG GIÁ TRỊ GIẢI THƯỞNG LÀ</span></b>
                                                    </td>
                                                    <td colspan="4"><b><span style="font-size: 12pt;">700.000</span></b></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4"><b><i><span
                                                                        style="font-size: 12pt;">Bảy trăm nghìn đồng chẵn </span></i></b>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <div class="simditor-resize-handle" contenteditable="false"></div>
                                        </div>
                                        <p>&nbsp;</p>
                                        <p style="text-align: justify;"><span style="font-size: 12pt;">(*): - Giải thưởng có thể thay đổi với giá trị bằng hoặc cao hơn tùy vào mỗi thời điểm để mang lại giá trị hấp dẫn cho người chơi.</span>
                                        </p>
                                        <p style="text-align: justify;"><span style="font-size: 12pt;">- Giải thưởng không được quy đổi thành tiền mặt.</span>
                                        </p>
                                        <p style="text-align: justify;"><span style="font-size: 12pt;">- Người nhận giải thưởng có nghĩa vụ nộp thuế Thu nhập cá nhân khi giá trị giải thưởng lớn hơn 10 triệu theo đúng quy định của pháp luật hiện hành. Khách hàng có thể nộp trực tiếp hoặc đồng ý khấu trừ luôn vào giá trị giải thưởng để Trúc Bạch nộp hộ.</span>
                                        </p>
                                        <p><span style="font-size: 13pt;">&nbsp;</span></p>
                                        <p><span style="font-size: 13pt;">&nbsp;</span></p>
                                        <p><span style="font-size: 13pt;">&nbsp;</span></p>
                                        <p><span style="font-size: 13pt;">&nbsp;</span></p>
                                        <p><span style="font-size: 13pt;">&nbsp;</span></p>
                                        <p><span style="font-size: 13pt;">&nbsp;</span></p>
                                        <p><span style="font-size: 13pt;">&nbsp;</span></p>
                                        <p><span style="font-size: 13pt;">&nbsp;</span></p>
                                        <p><span style="font-size: 13pt;">&nbsp;</span></p>
                                        <p><span style="font-size: 13pt;">&nbsp;</span></p>
                                        <p><span style="font-size: 13pt;">&nbsp;</span></p>
                                        <p><br></p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    @endsection

                    @section('scripts')

                    @endsection
                    @section('footer')

                    @endsection

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
@section('footer')

@endsection
