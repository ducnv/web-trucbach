@extends('layouts.app')

@section('title',  $category->seo_title)
@section('og_title',  $category->seo_title)
@section('description',  $category->seo_description)
@section('og_description',  $category->seo_description)
@section('keywords',  $category->seo_keywords)
@section('image',  $category->thumb)

@section('header')

@endsection

@section('content')
    <div class="wrapper"><!-- --->
        <div class="w_inner clearfix">
            <div class="col_content">

                @include('category.left')
                @include('category.right')

            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
@section('footer')

@endsection
