@extends('layouts.app')

@section('title',  "Hỏi đáp")
@section('og_title',  "Hỏi đáp")
@section('description', "Hỏi đáp")
@section('og_description', "Hỏi đáp")
@section('keywords',  "Hỏi đáp")

@section('header')
    <style>
        .table-bordered thead td, .table-bordered thead th {
            border-bottom-width: 2px;
        }
        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #dee2e6;
        }
        .table-bordered td, .table-bordered th {
            border: 1px solid #dee2e6;
        }
    </style>
@endsection

@section('content')
    <div class="wrapper"><!-- --->
        <div class="w_inner clearfix">
            <div class="col_content">
                <div class="simditor-body" >
                    @section('content')
                        <div class="wrapper"><!-- --->
                            <div class="w_inner clearfix">
                                <div class="col_content">
                                    <div class="simditor-body" >
                                        <p style="text-align: center;"><b><span style="font-size: 13pt;">HỎIĐÁP</span></b></p>
                                        <p><span style="font-size: 13pt;">&nbsp;<b>Để tham gia dịch vụ, tôi có cần thực hiện bước đăng ký nào không?</b></span>
                                        </p>
                                        <p style="text-align: justify;"><b><span style="font-size: 13pt;">TL:</span></b><span
                                                    style="font-size: 13pt;">&nbsp;Để đăng ký dịch vụ, Ban có thể thực hiện theo 2 cách. Cụ thể:</span>
                                        </p>
                                        <p style="text-align: justify;"><b><span style="font-size: 13pt;">Cách 1:</span></b><span
                                                    style="font-size: 13pt;">&nbsp;Đăng ký qua Tin nhắn SMS bằng di động của bạn.</span></p>
                                        <p style="text-align: justify;"><span style="font-size: 13pt;">Để đăng ký dịch vụ, Bạn soạn tin nhắn theo cú pháp DK gửi 9092. </span>
                                        </p>
                                        <p style="text-align: justify;"><b><span style="font-size: 13pt;">Cách 2</span></b><span
                                                    style="font-size: 13pt;">: Đăng ký qua Wapsite</span></p>
                                        <p style="text-align: justify;"><span style="font-size: 13pt;">Để đăng ký, Bạn truy nhập địa chỉ:&nbsp;&nbsp;<u>http://sanlocvang.com.vn</u>.</span>
                                        </p>
                                        <p style="text-align: justify;"><span style="font-size: 13pt;">Nếu Bạn đang truy cập bằng 3g của Vinaphone, hệ thống tự nhận diện thuê bao. Bạn chuyển tới nút “<u>Đăngký</u>”. Hệ thống sẽ hiện thị ra banner câu chào, yêu cầu bạn xác nhận để đăngký dịch vụ. Sau khi Bạn thực hiện các bước ấn lựa chọn trên banner dịch vụ, hệthống gửi tin nhắn báo về cho Bạn đã đăng ký thành công dịch vụ và Bạn có thểtham gia Săn lộc vàng ngay lúc này.</span>
                                        </p>
                                        <p style="text-align: justify;"><span style="font-size: 13pt;">Nếu Bạn có thêm thắc mắc, vui lòng liên hệ tổng đài: 9191 (200đ/ph)</span>
                                        </p>
                                        <p><span style="font-size: 13pt;">&nbsp;<b>Tôi có thể bị giới hạn nhắn bao nhiêu tin nhắn trong một ngày không?</b></span>
                                        </p>
                                        <p style="text-align: justify;"><b><span style="font-size: 13pt;">TL:</span></b><span
                                                    style="font-size: 13pt;">Bạn không bị giới hạn tin nhắn trong một ngày. </span></p>
                                        <p><span style="font-size: 13pt;">&nbsp;<b>Thời gian tham gia trong một ngày bắt đầu từ giờ nào và kết thúc khi nào?</b></span>
                                        </p>
                                        <p style="text-align: justify;"><b><span style="font-size: 13pt;">TL:</span></b><span
                                                    style="font-size: 13pt;">&nbsp;Hàng ngày, thời gian diễn ra dịch vụ sẽ bắt đầu từ 00:00:00’’ và kết thúc vào 23:59:59’’.</span>
                                        </p>
                                        <p><span style="font-size: 13pt;">&nbsp;<b>Ai sẽ được tham dự dịch vụ này?</b></span></p>
                                        <p><span style="font-size: 13pt;">&nbsp;Dịch vụ được cung cấp cho tất cả các thuê bao di động trả trước và trả sau đang hoạt động 2 chiều của VinaPhone. Hỗ trợ cho tất cả các loại điện thoại có thể sử dụng SMS.</span>
                                        </p>
                                        <p style="text-align: justify;"><span style="font-size: 13pt;">(*) Lưu ý: Toàn bộ cácthuê bao test, các thuê bao nghiệp vụ, các thuê bao Vinaphone miễn phí cước hoàn toàn, thuê bao của cán bộ công nhân viên đang làm việc tại Vinaphone và Công ty Trúc Bạch không được tham gia DỊCH VỤ này.</span>
                                        </p>
                                        <p><span style="font-size: 13pt;">&nbsp;<b>Làm thế nào để tôi biết về số lượng thời gian giữ Lộc vàng của tôi?</b></span>
                                        </p>
                                        <p style="margin-left: 0.5in; text-align: justify;"><b><span style="font-size: 13pt;">TL:</span></b>
                                        </p>
                                        <p><span style="font-size: 13pt;">&nbsp;Để kiểm tra&nbsp;<b>thời gian giữ Lộc vàng&nbsp;</b>trong ngày của mình,Bạn soạn tin nhắn:</span>
                                        </p>
                                        <p style="margin-left: 49.7pt; text-align: justify;"><b><span
                                                        style="font-size: 13pt;">KQ&nbsp;</span></b><span style="font-size: 13pt;">gửi&nbsp;<b>9092&nbsp;</b>(Miễn phí)</span>
                                        </p>
                                        <p><span style="font-size: 13pt;">&nbsp;<b>Khi tôi muốn được hướng dẫn tham gia dịch vụ, tôi phải làm thế nào?</b></span>
                                        </p>
                                        <p style="text-align: justify;"><b><span style="font-size: 13pt;">TL:</span></b><span
                                                    style="font-size: 13pt;">&nbsp;Quý khách hãy nhắn tin cú pháp HD gửi tới 9092 để được hướng dẫn tham gia dịch vụ, hoặc quý kháchcó thể truy cập website&nbsp;<u>http://sanlocvang.com.vn.</u>, hoặc gọi điện thoạitới tổng đài 9191 (200đ/ph) để được hướng dẫn chi tiết.</span>
                                        </p>
                                        <p><span style="font-size: 13pt;">&nbsp;<b>Sau bao lâu dịch vụ sẽ thông báo kết quả thuê bao chiến thắng lên website?</b></span>
                                        </p>
                                        <p style="text-align: justify;"><b><span style="font-size: 13pt;">TL:</span></b><span
                                                    style="font-size: 13pt;">&nbsp;&nbsp;</span></p>
                                        <p><span style="font-size: 13pt;">&nbsp;Giải tuần: Ban tổ chức (BTC) sẽ đăng tải thông tin 03 số thuê bao có tổng số thời gian giữ Lộc vàng lớn nhất tuần trên website&nbsp;<u>http://sanlocvang.com.vn&nbsp;</u>củaDỊCH VỤ vào ngày tiếp theo của tuần sau kết khi kết thúc tuần trước đó.</span>
                                        </p>
                                        <p><span style="font-size: 13pt;">&nbsp;<b>Thỉnh thoảng tôi đang chơi và sau đó không nhận được tin nhắn cảnh báo bị mất quyền giữ Lộc vàng và sau đó tôi phát hiện ra rằng Lộc vàng của tôi đã bị dành mất quyền giữ bởi một thuê bao khác. Vậy tại sao tôi lại gặp phải lỗi này? Và làm thế nào để giúp tôi biết chắc chắn rằng tôi vẫn đang còn quyền giữ Lộc vàng đó?</b></span>
                                        </p>
                                        <p style="text-align: justify;"><b><span style="font-size: 13pt;">TL:</span></b><span
                                                    style="font-size: 13pt;">&nbsp;Trong một số thời gian cao điểm, hệ thống phần mềm phải xử lý hàng nghìn câu lệnh cùng một lúc,có thể xảy ra xác xuất rất nhỏ khoảng 1% tin nhắn cảnh báo sẽ bị nghẽn và không được gửi tới máy của thuê bao nên thuê bao không được cảnh báo về việc Lộc vàng của Bạn đã bị thuê bao khác giành quyền giữ. Đây là một sự cố có xác suất nhỏ có thể xảy ra ngoài mong muốn của dịch vụ. &nbsp;Trong 1 số trường hợp khác, dosự cố về đường truyền mạng tại nơi Bạn đang sinh sống hoặc sự cố mạng</span>
                                        </p>
                                        <p style="text-align: justify;"><span style="font-size: 13pt;">Để tránh rủi ro không mong muốn này, thuê bao có thể soạn KT gửi 9092 để kiểm tra xem mình có đang là người giữ Lộc vàng hay không. Tin nhắn tra cứu KT gửi 9092 là miễn phí.</span>
                                        </p>
                                        <p><span style="font-size: 13pt;">&nbsp;</span></p>
                                        <p><span style="font-size: 13pt;">&nbsp;</span></p>
                                        <p><span style="font-size: 13pt;">&nbsp;</span></p>
                                        <p><span style="font-size: 13pt;">&nbsp;</span></p>
                                        <p><span style="font-size: 13pt;"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endsection

                    @section('scripts')

                    @endsection
                    @section('footer')

                    @endsection

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
@section('footer')

@endsection
