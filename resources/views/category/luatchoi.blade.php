@extends('layouts.app')

@section('title',  "Luật chơi")
@section('og_title',  "Luật chơi")
@section('description',  "Luật chơi")
@section('og_description',  "Luật chơi")
@section('keywords',  "Luật chơi")

@section('header')
    <style>
        .table-bordered thead td, .table-bordered thead th {
            border-bottom-width: 2px;
        }

        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #dee2e6;
        }

        .table-bordered td, .table-bordered th {
            border: 1px solid #dee2e6;
        }
    </style>
@endsection

@section('content')
    <div class="wrapper"><!-- --->
        <div class="w_inner clearfix">
            <div class="col_content">
                <div class="simditor-body">
                    <p>
                        {{--@if($msisdn)--}}
                            <span style="font-size: 13pt;">Xin chào: {{$msisdn ? $msisdn : 'bạn'}}. Mời bạn click&nbsp;</span>
                            <a target="_blank" href="{{$urlRg}}" class="">
                                <span style="font-size: 13pt; color: rgb(192, 48, 53);">Vào đây</span>
                            </a><span style="font-size: 13pt;">&nbsp;để đăng kí.</span>
                            {{--@elseif(checkHDH() == 'ios')--}}
                            {{--<span style="font-size: 13pt;">Xin chào: {{$msisdn}}. Mời bạn click&nbsp;</span>--}}
                            {{--<a href="sms:9092&body=DK" class="">--}}
                            {{--<span style="font-size: 13pt; color: rgb(192, 48, 53);">Vào đây</span>--}}
                            {{--</a><span style="font-size: 13pt;">&nbsp;để đăng kí.</span>--}}
                            {{--@else--}}
                            {{--<span style="font-size: 13pt;">Xin chào: bạn. Mời bạn click&nbsp;</span>--}}
                            {{--<a href="http://glive.vn/register.xhtml?reg=true&amp;pkg=1001143" class="">--}}
                            {{--<span style="font-size: 13pt; color: rgb(192, 48, 53);">Vào đây</span>--}}
                            {{--</a><span style="font-size: 13pt;">&nbsp;để đăng kí.</span>--}}
                        {{--@endif--}}
                    </p>
                    <p><b><span style="font-size: 13pt; color: rgb(104, 104, 104);">Giữ lộc liền tay, ring ngay thưởng lớn</span></b>
                    </p>
                    <p>
                        <span style="font-size: 13pt;">Săn lộc Vàng là dịch vụ Topup giải trí SMS trên đầu số 9092.</span>
                    </p>
                    <h1 style="text-align: center;"><b><span
                                    style="font-size: 13pt; color: rgb(64, 64, 64);">Luật chơi</span></b></h1>
                    <p style="text-align: center;"><span style="font-size: 13pt;">Để tham gia, người chơi phải đăng ký dịch vụ. Cú pháp : DK gửi 9092.<br>Để giành lại lộc vàng, soạn cú pháp LOC gửi 9092.<br></span><span
                                style="font-size: 13pt; color: rgb(204, 0, 51);">Người có thời gian chiếm giữ Lộc vàng lâu nhất sẽ trở thành người chiến thắng.</span>

                    </p>
                    <p style="text-align: center;">
                        {{--@if(checkHDH() == 'android')--}}
                            <span style="font-size: 13pt;"> Để Huỷ dịch vụ, soạn HUY SLV gửi 9092 hoặc bấm </span>
                            <a target="_blank" href="{{$urlHuy}}" class="">
                                <span style="font-size: 13pt; color: rgb(192, 48, 53);">vào đây</span>
                            </a>
                        {{--@elseif(checkHDH() == 'ios')--}}
                            {{--<span style="font-size: 13pt;">Để Huỷ dịch vụ, soạn HUY SLV gửi 9092 hoặc bấm&nbsp;</span>--}}
                            {{--<a href="sms:9092&body=HUY SLV" class="">--}}
                                {{--<span style="font-size: 13pt; color: rgb(192, 48, 53);">Vào đây</span>--}}
                            {{--</a>--}}
                        {{--@else--}}
                            {{--Để Huỷ dịch vụ, soạn HUY SLV gửi 9092 hoặc bấm “vào đây”--}}
                        {{--@endif--}}

                    </p>
                    <p style="text-align: center;"><span style="font-size: 13pt; color: rgb(204, 0, 51);">&nbsp;</span>
                    </p>
                    <p style="text-align: center;"><span style="font-size: 13pt; color: rgb(204, 0, 51);">&nbsp;</span>
                    </p>
                    <p style="text-align: center;"><span style="font-size: 13pt; color: rgb(204, 0, 51);">&nbsp;</span>
                    </p>
                    <p style="text-align: center;"><span style="font-size: 13pt; color: rgb(204, 0, 51);">&nbsp;</span>
                    </p>
                    <p style="text-align: center;"><span style="font-size: 13pt; color: rgb(204, 0, 51);">&nbsp;</span>
                    </p>
                    <p style="text-align: center;"><span style="font-size: 13pt; color: rgb(204, 0, 51);">&nbsp;</span>
                    </p>
                    <p style="text-align: center;"><span style="font-size: 13pt; color: rgb(204, 0, 51);">&nbsp;</span>
                    </p>
                    <p style="text-align: center;"><span style="font-size: 13pt; color: rgb(204, 0, 51);">&nbsp;</span>
                    </p>
                    <p style="text-align: center;"><span style="font-size: 13pt; color: rgb(204, 0, 51);">&nbsp;</span>
                    </p>
                    <p style="text-align: center;"><span style="font-size: 13pt; color: rgb(204, 0, 51);">&nbsp;</span>
                    </p>
                    <p style="text-align: center;"><span style="font-size: 13pt; color: rgb(204, 0, 51);">&nbsp;</span>
                    </p>
                    <p style="text-align: center;"><span style="font-size: 13pt; color: rgb(204, 0, 51);">&nbsp;</span>
                    </p>
                    <p style="text-align: center;"><span style="font-size: 13pt; color: rgb(204, 0, 51);">&nbsp;</span>
                    </p>
                    <p style="text-align: center;"><span style="font-size: 13pt; color: rgb(204, 0, 51);">&nbsp;</span>
                    </p>
                    <p style="text-align: center;"><span style="font-size: 13pt; color: rgb(204, 0, 51);">&nbsp;</span>
                    </p>
                    <p style="text-align: center;"><span style="font-size: 13pt; color: rgb(204, 0, 51);">&nbsp;</span>
                    </p>
                    <p style="text-align: center;"><span style="font-size: 13pt; color: rgb(204, 0, 51);">&nbsp;</span>
                    </p>
                    <p style="text-align: center;"><span style="font-size: 13pt; color: rgb(204, 0, 51);">&nbsp;</span>
                    </p>
                    <p style="text-align: center;"><span style="font-size: 13pt; color: rgb(204, 0, 51);">&nbsp;</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
@section('footer')

@endsection
