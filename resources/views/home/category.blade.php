@foreach($categories as $cat)
    <div class="box_category">
        <div class="title_box">
            <h3 class="txt_title"><a href="/{{$cat->slug}}">{{$cat->name}}</a>
            </h3>
        </div>
        <div class="content_box box_1">
            <div class="w_left_50">
                <div class="w_100">
                    @foreach($cat->article()->where('status','yes')->get()->slice(0,1) as $item)
                        <div class="block_image_news">
                            <div class="thumb">
                                <a href="{{getslug($item)}}" class="avatar">
                                    <img src="{{$item->thumb}}" alt=" {{$item->title}}">
                                </a>
                            </div>
                            <h3 class="title_news"><a href="#" class="title Article">
                                    {{$item->title}}
                                </a>
                            </h3>
                            <div class="news_lead">
                                {{$item->description}}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="w_right_50">
                <div class="w_100">
                    <ul class="list_news list_right list_txt clearfix">
                        @foreach($cat->article()->where('status','yes')->get()->slice(1,5) as $item)
                            <li>
                                <div class="block_image_news">
                                    <h3 class="title_news"><a
                                                href="{{getslug($item)}}"
                                                class="title Article">
                                            {{$item->title}}
                                        </a>
                                    </h3>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endforeach
