@extends('layouts.app')

@section('header')

@endsection

@section('content')
    <div class="wrapper">
        <div class="w_inner clearfix">
            @include('home.tophot')
            <div class="col_content">
                @include('home.left')
                @include('home.right')
            </div>
        </div>

    </div>
@endsection

@section('scripts')

@endsection
@section('footer')

@endsection
