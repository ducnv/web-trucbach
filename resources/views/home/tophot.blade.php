<div class="col_content_top">
    <div class="col-left">
        <div class="box_news_top">
            <div class="box_hot_news">
                <div class="row_1">
                    <div class="box_news_big"><a href="{{getslug($news[0])}}" class="avatar">
                            <img src="{{$news[0]->thumb}}" alt="{{$news[0]->title}}">
                        </a>
                    </div>
                    <h1 class="title_news"><a href="{{getslug($news[0])}}" class="title Article">
                            {{$news[0]->title}}
                        </a>
                    </h1>
                    <h4 class="news_lead">
                        {{$news[0]->description}}
                    </h4>
                </div>
            </div>
            <div class="box_sub_hot_new">
                <style>.tabs_new_top ul.tabs li {
                        display: inline;
                        padding-left: 10px;
                    }</style>
                <div class="tabs_new_top ui-tabs ui-corner-all ui-widget ui-widget-content">
                    <ul class="tabs ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header">
                        <li class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active">
                            <a class="ui-tabs-anchor" href="#tabs-1">MỚI NHẤT</a>
                        </li>
                        <li class="ui-tabs-tab ui-corner-top ui-state-default ui-tab"><a
                                    class="ui-tabs-anchor" href="#tabs-2">ĐỌC NHIỀU </a>
                        </li>
                    </ul>
                    <div id="tabs-1" class="ui-tabs-panel ui-corner-bottom ui-widget-content">
                        <div class="scroll_hot_list">
                            <ul class="hot_list">
                                @foreach($news as $item)
                                    <li>
                                        <div class="title_news"><a
                                                    href="{{getslug($item)}}"
                                                    class="title Article">{{$item->title}}</a>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="line_clear"></div>
                    </div>
                    <div id="tabs-2" style="display: none;">
                        <div class="scroll_hot_list">
                            <ul class="hot_list">
                                @foreach($news as $item)
                                    <li>
                                        <div class="title_news"><a
                                                    href="{{getslug($item)}}"
                                                    class="title Article">{{$item->title}}</a>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="line_clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-right">
    </div>
</div>
