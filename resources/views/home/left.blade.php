<div class="col-left">
    <div class="box_hot_news">
        <div class="row_2">
            <div class="box_featured_news">
                <div class="featured_content">
                    <ul class="list_news clearfix">
                        @foreach($news->slice(1,6) as $item)
                            <li>
                                <div class="block_image_news">
                                    <div class="thumb"><a
                                                href="{{getslug($item)}}"
                                                class="avatar"><img
                                                    src="{{$item->thumb}}"
                                                    alt="{{$item->title}}">
                                        </a>
                                    </div>
                                    <h3 class="title_news"><a
                                                href="{{getslug($item)}}"
                                                class="title Article">{{$item->title}}</a>
                                    </h3>
                                    <div class="news_lead cut_string" data-limit="150">
                                        {{$item->description}}
                                    </div>
                                    <div style="display: none;">
                                        {{$item->description}}
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="box_sub_hot_new">
        <div class="box_bg_hot_new">
            <div class="scroll_other_list">
                <ul class="hot_list other_list">
                    @foreach($news->slice(1,5) as $item)
                    <li>
                        <div class="block_image_news">
                            <div class="thumb"><a
                                        href="{{getslug($item)}}"
                                        class="avatar"><img
                                            src="{{$item->thumb}}"
                                            alt="{{$item->title}}"
                                            style="height: 120px;">
                                </a>
                            </div>
                            <h3 class="title_news"><a
                                        href="{{getslug($item)}}"
                                        class="title Article">{{$item->title}}</a>
                            </h3>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@include('home.category')
</div>
