<div class="col-left" itemscope="1" itemtype="http://schema.org/Article">
    <ul id="submenu_nav" class="breadcrumb clearfix">
        <li class="active"><a class="dropbtn" href="/{{$category->slug}}">{{$category->name}}</a>
        </li>
    </ul>
    <div class="block_col">
        <div class="block_col_right">
            <div class="block-news-right"><h1 class="head_title">{{$article->title}}</h1>
                <div class="box_support clearfix">
                    <div class="date_time"><span class="date">{{$article->created_at}}</span>
                    </div>
                    <div class="block_share right">
                    </div>
                </div>
                <div class="content-detail">
                    <p style="text-align: justify;"><strong>
                            {{$article->description}}</strong>
                    </p>
                        {!! $article->body !!}
                </div>
                <link rel="stylesheet" type="text/css"
                      href="http://vnmedia.vn/common/v2/css/tinanh.css"/>
                <style>.photo_type .des {
                        font-size: 13px;
                        line-height: 18px;
                    }</style>

                <script type="text/javascript" src="http://vnmedia.vn/common/v2/js/tinanh.js"></script>
                <script type="text/javascript"
                        src="http://vnmedia.vn/common/jquery/jquery.fullscreen-min.js"></script>
                <script type="text/javascript">
                    $(document).ready(function () {
                        var photoContent = new contentInit();
                        photoContent.startup();
                    });
                </script>

                <style type="text/css">
                    .main-qc {
                        border: 1px solid #ccc;
                    }
                </style>
                <div class="box_tag">
                    <div class="txt_tag"><span>Tags</span>
                    </div>
                </div>
                <link href="/common/v6/css/feedback.css" rel="stylesheet">

                <script src="/common/v6/js/jquery.paginate.js">.</script>
                {{--<script src="/common/v6/js/feedback.js">.</script>--}}

            </div>
        </div>
        <div class="block_col_left">
            <div class="block_news_left">
                <h3 class="title_box"><span>Mới nhất</span>
                </h3>
                <ul>
                    @foreach($lastNews as $k => $item)
                        <li>
                            <div class="block_image_news">
                                <div class="thumb"><a
                                            href="{{getslug($item)}}"><img
                                                src="{{$item->thumb}}"
                                                alt="{{$item->title}}">
                                    </a>
                                </div>
                                <div class="news_lead"><a
                                            href="{{getslug($item)}}"
                                            class="title Article">{{$item->title}}</a>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="block_news_left block_read_more">
                <h3 class="title_box"><span>Đọc nhiều</span>
                </h3>
                <ul>
                    @foreach($reads as $k => $item)
                        @if($k === 0)
                            <li class="first">
                                <div class="block_image_news">
                                    <div class="thumb"><a
                                                href="{{getslug($item)}}"><img
                                                    src="{{$item->thumb}}"
                                                    alt="{{$item->title}}">
                                        </a>
                                    </div>
                                    <div class="news_lead"><a
                                                href="{{getslug($item)}}"
                                                class="title Article">{{$item->title}}</a>
                                    </div>
                                </div>
                            </li>
                        @else
                            <li>
                                <div class="news_lead"><a
                                            href="{{getslug($item)}}"
                                            class="title Article">{{$item->title}}</a>
                                </div>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <div class="block_new_related">
        <h3 class="title-categories">CÙNG CHUYÊN MỤC</h3>
        <div class="list_item">
            <div class="block_items clearfix">
                @foreach($newRelations->slice(0,4) as $item)
                    <div class="item">
                        <div class="block_image_news">
                            <div class="thumb"><a
                                        href="{{getslug($item)}}"
                                        class="avatar"><img
                                            src="{{$item->thumb}}"
                                            alt="{{$item->title}}">
                                </a>
                            </div>
                            <h3 class="title_news"><a
                                        href="{{getslug($item)}}"
                                        class="title ">{{$item->title}}</a>
                            </h3>
                            <div class="news_lead">{{$item->description}}
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="block_items clearfix">
                    @foreach($newRelations->slice(4,4) as $item)
                        <div class="item">
                            <div class="block_image_news">
                                <div class="thumb"><a
                                            href="{{getslug($item)}}"
                                            class="avatar"><img
                                                src="{{$item->thumb}}"
                                                alt="{{$item->title}}">
                                    </a>
                                </div>
                                <h3 class="title_news"><a
                                            href="{{getslug($item)}}"
                                            class="title ">{{$item->title}}</a>
                                </h3>
                                <div class="news_lead">{{$item->description}}
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
