@extends('layouts.app')
@section('title',  $article->title)
@section('og_title',  $article->title)
@section('description',  $article->description)
@section('og_description',  $article->description)
@section('keywords',  $article->title)
@section('og_type',  "article")
@section('image',  $article->thumb)
@section('header')
    <style>
        div#photo_type_content {
            display: block !important;
        }
    </style>

@endsection

@section('content')
    <div class="wrapper">
        <div class="w_inner clearfix">
            <div class="col_content">

                @include('news.left')
                @include('news.right')
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
@section('footer')

@endsection
