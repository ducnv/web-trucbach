<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/profile', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['auth:api']], function () {
    // USER
    Route::get('/users', 'UserController@apiGetUsers');
    Route::post('/user', 'UserController@apiCreateUser');
    Route::put('/user/{id}', 'UserController@apiUpdateUser');
    Route::get('/roles', 'UserController@apiGetRoles');
    Route::get('/permissions', 'UserController@getPermissions');
    Route::put('/roles/update-permissions', 'UserController@roleUpdatePermissions');

    //carrer
    Route::get('/carrer', 'CarrerController@apiGetCarrer');
    Route::post('/carrer/create', 'CarrerController@postCarrer');
    Route::put('/carrer/update', 'CarrerController@putCarrer');

    //salary
    Route::get('/salary', 'SalaryController@apiGetSalary');
    Route::post('/salary/create', 'SalaryController@postSalary');
    Route::put('/salary/update', 'SalaryController@putSalary');

    //level
    Route::get('/level', 'LevelController@apiGetLevel');
    Route::post('/level/create', 'LevelController@postLevel');
    Route::put('/level/update', 'LevelController@putLevel');
    //experience
    Route::get('/experience', 'ExperienceController@apiGetExperience');
    Route::post('/experience/create', 'ExperienceController@postExperience');
    Route::put('/experience/update', 'ExperienceController@putExperience');
    //position
    Route::get('/position', 'PositionController@apiGetPosition');
    Route::post('/position/create', 'PositionController@postPosition');
    Route::put('/position/update', 'PositionController@putPosition');
    //industrialzone
    Route::get('/industrialzone', 'IndustrialZoneController@apiGetIndustrialZone');
    Route::post('/industrialzone/create', 'IndustrialZoneController@postIndustrialZone');
    Route::put('/industrialzone/update', 'IndustrialZoneController@putIndustrialZone');
    Route::get('/industrialzone/province', 'IndustrialZoneController@apiGetProvince');

    //industrialzone
    Route::get('/company', 'CompanyController@apiGetCompany');
    Route::post('/company/create', 'CompanyController@postCompany');
    Route::put('/company/update', 'CompanyController@putCompany');
    //category
    Route::get('/category', 'CategoryController@apiGetCategory');
    Route::post('/category/create', 'CategoryController@postCategory');
    Route::put('/category/update', 'CategoryController@putCategory');
    //news
    Route::get('/news', 'NewsController@apiGetNews');
    Route::post('/news/create', 'NewsController@postNews');
    Route::put('/news/update', 'NewsController@putNews');
    Route::get('/news/category', 'NewsController@apiGetCategory');
    //jobpost
    Route::get('/job/type', 'JobPostController@apiGetType');
    Route::get('/job/level', 'JobPostController@apiGetLevel');
    Route::get('/job/exper', 'JobPostController@apiGetExper');
    Route::get('/job/salary', 'JobPostController@apiGetSalary');
    Route::get('/job/position', 'JobPostController@apiGetPosition');
    Route::get('/job/carrer', 'JobPostController@apiGetCarrer');
    Route::get('/job/company', 'JobPostController@apiGetCompany');
    Route::post('/job/create', 'JobPostController@postJobPost');
    Route::put('/job/update', 'JobPostController@putJobPost');
    Route::get('/job', 'JobPostController@apiGetJob');

    Route::get('/media', 'MediaController@getMedia');
    Route::post('media', 'MediaController@createMedia')->name('api.post_media');
    Route::put('media/{id}', 'MediaController@updateMedia')->name('api.put_media');
    Route::post('mediacloselogo', 'MediaController@mediaCloseLogo')->name('api.media_close_logo');
    Route::post('delete/media', 'MediaController@deleteMedia')->name('api.delete_media');

    // CRAWLER
    Route::get('/crawl', 'CrawlerController@apiGetCrawl');
    Route::get('/crawl/province', 'CrawlerController@apiGetProvince');
    Route::get('/crawl/district', 'CrawlerController@apiGetDistrict');
    Route::get('/crawl/industrial-zone', 'CrawlerController@apiGetIndustrialZone');
    Route::get('/crawl/carrers', 'CrawlerController@apiGetCarrer');
    Route::get('/crawl/keywords', 'CrawlerController@apiGetKeywords');
    Route::get('/crawl/site-crawler', 'CrawlerController@apiGetSiteCrawler');
    Route::post('/crawl/create-site-crawler', 'CrawlerController@postSiteCrawler');
    Route::put('/crawl/update-site-crawler', 'CrawlerController@putSiteCrawler');
    Route::post('/crawl/create-category', 'CrawlerController@postCategory');
    Route::put('/crawl/update-category', 'CrawlerController@putCategory');
    Route::get('/crawl/category', 'CrawlerController@apiGetCategory');

    Route::get('/crawl/company', 'CrawlerController@apiGetCompany');

    Route::put('/crawl/update-job', 'CrawlerController@putJobPost');
    Route::put('/crawl/create-job', 'CrawlerController@postJobPost');

    Route::put('/crawl/update-company', 'CrawlerController@putCompany');
    Route::put('/crawl/create-company', 'CrawlerController@postCompany');

    Route::get('/crawl/site-client', 'CrawlerController@apiGetSiteClient');
    Route::post('/crawl/create-site-client', 'CrawlerController@postSiteClient');
    Route::put('/crawl/update-site-client', 'CrawlerController@putSiteClient');
    Route::put('/crawl/add-category-siteclient', 'CrawlerController@putCategorySiteClient');

    Route::post('/crawl/publish-job', 'CrawlerController@postPublishJob');
    Route::delete('/crawl/delete-job/{id}', 'CrawlerController@deleteJob');

});