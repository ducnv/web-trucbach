<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::get('/chinh-sach-bao-ve-thong-tin-khach-hang', 'CategoryController@chinhSach')->name('chinhSach');

Route::get('/luat-choi', 'CategoryController@luatChoi')->name('luatChoi');

Route::get('/huong-dan', 'CategoryController@huongDan')->name('huongDan');

Route::get('/hoi-dap', 'CategoryController@hoiDap')->name('hoiDap');

Route::get('/giai-thuong', 'CategoryController@giaiThuong')->name('giaiThuong');

Route::get('/test', 'CategoryController@test')->name('test');

Route::get('/{slug}', 'CategoryController@index')->name('category');

Route::get('/{cat_slug}/{slug}.html', 'NewsController@index')->name('news');


