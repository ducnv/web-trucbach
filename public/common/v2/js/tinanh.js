var vsAdData = {};

function get_url_params(name) {
	name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	var regexS = "[\\?&]" + name + "=([^#&]*)";
	var regex = new RegExp( regexS );
	var results = regex.exec(window.location.href);
	if( results == null ) {
	return "";
	}
	else {
	return decodeURI(results[1].split('+').join(' '));
	}
}

function statsSiteOfVS(stats_url, site_name) {
	try {
		var stats_src = stats_url + "?u=2&s=@SITE-ID@&c=@CATE-ID@&p=@ARTICLE-ID@&t=@TITLE@&cp=" + Math.random();
		stats_src = stats_src.replace('@SITE-ID@', getInnerHTML('site-id'));
		if (getInnerHTML('cate-id') != "" ) {
			stats_src = stats_src.replace('@CATE-ID@', getInnerHTML('cate-id'));
			if (getInnerHTML('article-id') != "" ) {
				stats_src = stats_src.replace('@ARTICLE-ID@', getInnerHTML('article-id'));
				stats_src = stats_src.replace('@TITLE@', getInnerHTML('article-title'));
			}else {
				stats_src = stats_src.replace('@ARTICLE-ID@', 0);
				stats_src = stats_src.replace('@TITLE@', getInnerHTML('cate-title'));
			}
		}else {
			stats_src = stats_src.replace('@CATE-ID@', 0);
			stats_src = stats_src.replace('@ARTICLE-ID@', 0);
			stats_src = stats_src.replace('@TITLE@', site_name);
		}

		var stat_img = document.createElement('img');
		stat_img.setAttribute('src', stats_src);
		stat_img.setAttribute('width', 0);
		stat_img.setAttribute('height', 0);
		document.body.appendChild(stat_img);
	}catch (e) {}

	function getInnerHTML(id) {
		return document.getElementById(id).innerHTML;
	}
}

var userAgent = navigator.userAgent.toLowerCase();
photoIndex = get_url_params('p');
if (photoIndex == ''){
	photoIndex = 1;
}

$("#article #content img").hide();
fullScreen = false;
var objImage = $("#content img");
objImage = jQuery.grep(objImage, function( n, i ) {
	return ( $(n).attr('class') != 'logo' );
});

var automessage;

function contentInit() {
	var self = this;
	this.startup = function() {
		//this.turnOn();
		this.genContent(photoIndex, 10);

		//this.onchange();
	}
	this.imgHeight = function () {
		return (parseInt($(window).height()) - (parseInt($('.photo_type .image_des').outerHeight()) + parseInt(108))) + 'px';
	}
	this.imgPad = function () {
		return (parseInt($(window).height()) - (parseInt($('.photo_type img').height()) + parseInt($('.photo_type .image_des').outerHeight()) + parseInt($('.photo_type .list_thumbnail').outerHeight())))/2 + 'px';
	}
	this.genContent = function (paged) {
		$(".content-detail p")
$.each( $(".content-detail p"), function( key, value ) {
  if($(this).html() == '&nbsp;') $(this).remove();
});

		if ($('.photo_type').length == 0){
			var strTemp =
				'<div class="photo_type">'+
				'<a href="http://www.facebook.com/share.php?u=' + window.location.href + '" class="facebook" target="_blank" title="Đăng lên Facebook"></a>'+
				'<div title="Xem toàn màn hình" id="fullscreen"></div>'+
				'<div id="photo_type_content" style="display: none">'+
					'<div class="image">'+
						'<div class="paged prev" title="Ảnh trước"></div>'+
						'<div class="paged next" title="Ảnh sau"></div>'+
						'<img src="" />'+
						'<div class="page_number"><span class="number"></span><span style="padding-left: 15px;cursor: pointer;position: relative;z-index: 99"  class="auto_slide">Xem slide</span></div>'+
					'</div>'+
					'<div class="image_des"></div>'+
					'<div class="list_thumbnail"></div>'+
				'</div>'+
			'</div>';
			if ($("#content table.image")[0]) {
				$(strTemp).insertBefore($($("#content table.image")[0]));
			} else {
				$(strTemp).insertAfter($($("#content").children()[0]));
			}
			//self.hover();
			self.clickAuto();
			self.fullscreen();
			setTimeout(function(){
				self.listCarousel();
			}, 600);
			$('.photo_type .paged').click(function() {
				self.onchange($(this));
			})
		}

		$("#photo_type_content").hide();
		$(".photo_type .paged").show();
		paged =  parseInt(paged);
		strImg = '';
		for (i = 0; i < objImage.length; i++) {
		if (i == (paged - 1)) {
				var desImage = '';
				if ($(objImage[i]).attr('alt')) {
					desImage = '<div class="des">' + $(objImage[i]).attr('alt') + '</div>';
				}

				$("#photo_type_content .image img").attr('src', $(objImage[i]).attr('src'));
				$("#photo_type_content .image_des").html(desImage);
				$("#photo_type_content").fadeIn( "fast" );
				$(".photo_type .page_number .number").html('Ảnh ' + paged + '/' + objImage.length);
			}
		}

		$(".photo_type .prev").attr('value', paged - 1);
		$(".photo_type .next").attr('value', paged + 1);

		if (paged == (objImage.length)) {
			self.turnOff();
			$(".photo_type .prev").attr('value', 1);
			//self.turnOn();
		}

		if (paged == 1) {
			$(".photo_type .prev").attr('value', '');
			$(".photo_type .prev").hide();
		}

		//$('.photo_type img').css('max-height', self.imgHeight());

		if (vsAdData["ad_slide_3"] && paged > 1 && paged < 5) {
			if ($('#photo_type_content .image .ad_center').length == 0) {
				$('#photo_type_content .image').append('<div class="ad_center" style="display: none; min-height: 250px;"><div id="ad_slide_3" class="ad_slide ad_webtube_center_6" style="display: none" name="vsad_border"></div></div>')
				try{load_ads();} catch (e){}
			}
		} else {
			$('.col1 .ad_center').remove();
		}

		if (vsAdData["ad_slide_3"] && paged == 3) {
			$('#photo_type_content .image .ad_center').show();
			$('.photo_type .image img').hide();
			$('.photo_type .image_des').hide();
			$(".photo_type .paged").addClass("hasAd");
		} else {
			$('#photo_type_content .image .ad_center').hide();
			$('.photo_type .image img').show();
			$('.photo_type .image_des').show();
			$(".photo_type .paged").removeClass("hasAd");
		}

		if (paged == (objImage.length)) {
			if (vsAdData["ad_slide_last"]) {
				$('#photo_type_content .image').append('<div class="ad_center ad_last" style="display: none; min-height: 250px;"><div id="ad_slide_last" class="ad_slide_last" style="display: none" name="vsad_border"></div></div>')
				try{load_ads();} catch (e){}
				$(".photo_type .next").addClass("showAd");
			}
		}

		if (!vsAdData["ad_slide_last"] && paged == (objImage.length)) {
			$(".photo_type .next").attr('value', '1');
			//$(".photo_type .next").hide();
		}

		self.activeCarousel(paged);
	}

	this.listCarousel = function () {
		var strHtml ='';
		strHtml += '<div id="content_1" class="carousel" style="width: ' + ($('.photo_type').width() - 20) + 'px"><ul id="foo3">';
		for (var i=0; i< objImage.length; i++) {
			if (i == (photoIndex - 1)) {
				strHtml += '<li id="mcs_t_5" class="active" paged="' + (i+1) + '"><a><img src="' + $(objImage[i]).attr('src') + '" /></a></li>';
			} else {
				strHtml += '<li paged="' + (i+1) + '"><a><img src="' + $(objImage[i]).attr('src') + '" /></a></li>';
			}

		}
		strHtml +=
			'</ul></div>';
		$(".list_thumbnail").html(strHtml);

		$('.carousel li').click(function () {
			var url = 'http://' + (location.host + location.pathname)+ '?p=' + $(this).attr('paged');
			self.pushStateUrl($(this).attr('paged'), url);
		})

		$.ajax({
			url: 'http://megafun.vn/common/v3/jquery/jquery.carouFredSel-6.2.1-packed.js',
			dataType: "script",
			cache: true
		}).done(function() {
			setTimeout(function(){
				$('.carousel #foo3').carouFredSel({
					auto: false,
					items: 5
				});
				self.slideTo(photoIndex);
			}, 600);
		});
	}

	this.activeCarousel = function (paged) {
		$.each($('.carousel  ul').children(), function(index, photo) {
			$(photo).attr('id', '');
			$(photo).attr('class', '');
			if ($(photo).attr('paged') == paged) {
				$(photo).attr('id', 'mcs_t_5');
				$(photo).attr('class', 'active');
			}
		});
	}

	this.onchange = function (attachment) {
		if (attachment.attr('value') != '') {
			if (attachment.hasClass('showAd')) {
				$('#photo_type_content .image .ad_center').show();
				$('.photo_type .image img').hide();
				$(".photo_type .next").hide();
				attachment.removeClass('showAd');
				$(".photo_type .paged").addClass('hasAd');
				$('.photo_type .image_des').hide();
				self.turnOff();
			} else {
				if (attachment.hasClass( "hasAd" )) {
					$('#photo_type_content .image .ad_center').hide();
					$('.photo_type .image img').show();
					$('.photo_type .image_des').show();
					$(".photo_type .paged").show();
					$(".photo_type .paged").removeClass("hasAd");

					if (attachment.hasClass( "prev" ) && attachment.attr('value') == (objImage.length - 1)) {
						$(".photo_type .next").addClass("showAd");
					}
				} else {
					$('.photo_type .image_des').show();
					$(".photo_type .next").removeClass('showAd');
					$('.photo_type .image img').show();
					$(window).scrollTop($(".photo_type").offset().top);
					var url = 'http://' + (location.host + location.pathname)+ '?p=' + attachment.attr('value');
					self.pushStateUrl(attachment.attr('value'), url);
				}
			}
		}

	}

	this.slideTo = function (paged) {
		if (paged > 3 && paged < (objImage.length) - 1) {
			$('.carousel #foo3').trigger('slideTo', (paged - 3));
		}
		if (paged == 1 || paged == '') {
			$('.carousel #foo3').trigger('slideTo', (paged - 1));
		}
		if (paged == (objImage.length)) {
			$('.carousel #foo3').trigger('slideTo', (paged - 5));
		}
		if (paged == (objImage.length - 1)) {
			$('.carousel #foo3').trigger('slideTo', (paged - 4));
		}
		if (paged == (objImage.length - 2)) {
			$('.carousel #foo3').trigger('slideTo', (paged - 3));
		}
	}

	this.pushStateUrl = function (paged, url, back) {
		if (paged && paged != 'NaN' && paged <= (objImage.length + 1)) {
			self.slideTo(paged);

			if ((userAgent.indexOf('chrome') > -1) && fullScreen) {
				self.genContent(paged)
			} else {
				history.pushState(self.genContent(paged, 10, back), '', url); //log {self.filterContent(paged, 10}, page_title, url
				head.js('https://www.google-analytics.com/ga.js', function() {
					_gaq.push(['vs._setAccount', 'UA-65945825-1']);
					_gaq.push(['vs._trackPageview']);
				});
				

				statsSiteOfVS('http://123.29.67.238/service/statistic/statcollector.do', 'Xa&#771; h&#244;&#803;i th&#244;ng tin');

			}
		} else {
			//self.turnOff();
		}
	}

	this.hover = function () {

		$( ".article-content .photo_type" )
		.mouseenter(function() {
			self.turnOff();
		})
		.mouseleave(function() {
			if ($('.photo_type .next').attr('value') != '') {
				self.turnOn();
			} else {
				self.turnOff();
			}
		});
	}

	this.clickAuto = function () {
		 $(".photo_type .auto_slide").click(function() {
			 if ($(this).html() == "Xem slide") {
				 self.turnOn();
				 $(this).html('Dừng slide');
			 } else {
				self.turnOff();
				$(this).html("Xem slide");
			 }
		 })
	}


	 this.turnOn = function () {
			automessage = setInterval(function(){
			self.onchange($(".photo_type .next"));
		   }, 6000);

	 }

	this.turnOff = function () {
	   clearInterval(automessage);
	 }

	this.fullscreen = function () {
		$('#fullscreen').click(function() {
			$('.photo_type').toggleFullScreen();
		})
		$(function() {
			$(document).bind("fullscreenchange", function(e) {
				//$('.photo_type img').css('max-height', self.imgHeight());
				if ($(document).fullScreen()) {
					$('#fullscreen').attr('title', 'Thoát khỏi xem toàn màn hình');
					fullScreen = true;
				} else {
					$(window).scrollTop($(".photo_type").offset().top);
					fullScreen = false;
				}
			});
		});

	}
}