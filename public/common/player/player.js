var videoReader = 'http://ctl.cdn.truelife.vn/offica/video/';
var audioReader = 'http://ctl.cdn.truelife.vn/offica/audio/';
var skinForPlayer = "/common/player/skins/glow/glow.zip";
var height_toolbar = 250;
var player_height = 260;
var player_width =  460;
var avatar = '/common/v3/image/logo.png';

try {
	avatar = document.getElementById("avatar").innerHTML.replace('normal', 'original');
}
catch (e) {}

function setCookie(cookieName,cookieValue,nDays) {
	var today = new Date();
	var expire = new Date();
	if (nDays == null || nDays == 0) nDays = 1;
	expire.setTime(today.getTime() + 3600000*24*nDays);
	document.cookie = cookieName + "=" + escape(cookieValue) + ";expires=" + expire.toGMTString() + ";path=/";
}

function getCookie( name ) {
	var start = document.cookie.indexOf( name + "=" );
	var len = start + name.length + 1;
	if ( ( !start ) && ( name != document.cookie.substring( 0, name.length ) ) ) {
		return null;
	}
	if ( start == -1 ) return null;
	var end = document.cookie.indexOf( ";", len );
	if ( end == -1 ) end = document.cookie.length;
	return unescape( document.cookie.substring( len, end ) );
}
skinForPlayer = '';
if (typeof (isMobile) != 'undefined' && isMobile) {
	player_width = 300;
	height_toolbar = 24;
	var player_height = 200;
	var height_toolbar = 120;
}

function setup_player(confs) {
	if (!confs.id) {
		return;
	}

	var videoConfs = {
		image: '/common/v3/image/logo.png',
		width: player_width,
		height: player_height,
		autostart: false,
		controlbar: {
			position: 'bottom',
			idlehide: true
		},
		events: {
			onFullscreen: function(o) {
				try {
					if (o.fullscreen) {
						vs.get('ad_article_bottom_block').style.display = 'none';
					}else {
						vs.get('ad_article_bottom_block').style.display = 'block';
					}
				}
				catch (e) {}
			}
		}
	};

	var audioConfs = {
		image: avatar,
		provider: 'audio',
		width: 300,
		height: height_toolbar,
		autostart: false,
		file_name: confs.file,
		controlbar: {
			position: 'bottom',
			idlehide: false
		},
		modes: [{
			type: 'flash',
			src: 'http://megafun.vn/common/player/player.swf'
		},{
			type: 'html5'
		}]
	};

	switch (confs.type) {
		case 'audio':
					applyObject(confs, {plugins: {
						'related-1': {
							'file': 'http://' + location.host + location.pathname +'index.related.xml',
							'onclick': '_target',
							'usedock': false,
							'dimensions': '90x60',
							'heading': 'Các radio khác'
						}
						}
					});
			applyObject(confs, {
				file: [
					audioReader,
					'?file=',
					confs.file
				].join('')
			});

			applyObject(audioConfs, confs);
			jwplayer(confs.id).setup(audioConfs);
		break;

		case 'video':
			if (confs.file) {
				applyObject(confs, {
					modes: [{type: 'flash',
						src: 'http://megafun.vn/common/player/player.swf',
						config: {
							file: [
							 'http://s2.cp.megafun.vn/video',
							 confs.file
							].join('')
						}
					},{
						type: 'html5',
						config: {
							file: [
								 'http://s2.cp.megafun.vn/video',
								 confs.file
								].join(''),
							provider: 'video'
						}
					}]
				});
			}

			//confs.autostart = true;
			/*if (typeof (isMobile) != 'undefined' && !isMobile && $("#cate-id").html() != '9222') {*/
			if ((typeof (isMobile) == 'undefined')) {
				totalPreRoll = 4;
					if (getCookie('c_current_adver_preroll')) {
						current_adver_preroll = parseInt(getCookie('c_current_adver_preroll'));
						if (totalPreRoll > current_adver_preroll) {
							current_adver_preroll++;
						}else {
							current_adver_preroll = 1;
						}
					}else {
						current_adver_preroll = Math.floor((Math.random() * totalPreRoll) + 1);
						setCookie('c_current_adver_preroll', current_adver_preroll, 365);
					}

					setCookie('c_current_adver_preroll', current_adver_preroll, 365);

					plugins = {};
					//current_adver_preroll= 3;

					//console.log(current_adver_preroll)

					/*d = new Date();
					if (d.getHours() >= 14 && d.getHours() <= 16) {
						current_adver_preroll = 3;
					}else if (current_adver_preroll == 3 || current_adver_preroll == 2) {
						current_adver_preroll = 0;
					}*/

				    if (location.href.indexOf('http://auco.megafun.vn/vckm/editors/preview.jsp') >= 0) {
						current_adver_preroll= 100;
				    }

					switch (current_adver_preroll) {
					/*case 1:
						$('#' + confs.id).before('<div id="TVC_ADMICRO"></div>');
						$('#' + confs.id).remove();
						$('#TVC_ADMICRO')
							.html('<div id="'+confs.id+'"></div>')
							.attr('width', confs.width)
							.attr('height', confs.height);

						applyObject(confs, {plugins: {
							'related-1': {
								'file': 'http://' + location.host + location.pathname +'index.related.xml',
								'onclick': '_target',
								'usedock': false,
								'dimensions': '160x90',
								'heading': 'Các video khác'
							}
							}
						});
						break;*/
					case 2:
						confs.config = 'http://vnmedia.vn/common/player/pre-roll/bluesee.config.xml';
						applyObject(confs, {plugins: {
							'http://media.adnetwork.vn/flash/jwplayer/ova-jw.swf': {}
							}
						});
						break;
					case 10:
						confs.config = 'http://data.lavanetwork.net/www/delivery/fc.php?script=bannerTypeHtml:vastInlineBannerTypeHtml:vastInlineHtml&nz=1&&zones=pre-roll1%3D484%7&format=vast';
						applyObject(confs, {plugins: {
							'related-1': {
								'file': 'http://' + location.host + location.pathname +'index.related.xml',
								'onclick': '_target',
								'usedock': false,
								'dimensions': '160x90',
								'heading': 'Các video khác'
							}
							}
						});
						break;
					case 3:
						confs.config = 'http://megafun.vn/common/player/pre-roll/config.xml';
						applyObject(confs, {plugins: {
							'http://media.adnetwork.vn/flash/jwplayer/ova-jw.swf': {},
							'related-1': {
								'file': 'http://' + location.host + location.pathname +'index.related.xml',
								'onclick': '_target',
								'usedock': false,
								'dimensions': '160x90',
								'heading': 'Các video khác'
							}
							}
						});
					break;
					/*case 4:
						confs.config = '/common/player/pre-roll/innity.config.xml';
						applyObject(confs,{
							'http://plugin.innity.net/jw/InnityAdsPlugin.swf': {},
							'related-1': {
								'file': 'http://' + location.host + location.pathname +'index.related.xml',
								'onclick': '_target',
								'usedock': false,
								'dimensions': '160x90',
								'heading': 'Các video khác'
							}
						});
					break;*/
					case 1:
						applyObject(confs, {plugins: {
								'http://media.adnetwork.vn/flash/jwplayer/ova-jw.swf': {
									'overlays': {
										'regions': [
											{
												'id': 'AmbientDigitalNotice',
												'verticalAlign': 'bottom',
												'horizontalAlign': 'right',
												'backgroundColor': 'transparent',
												'width': '300',
												'height': '20',
												'style': '.abdNotice{font-size:11pt; font-family:Tahoma; font-style: italic;  color:#FFCC00;}'
											}
										]
									},
									'ads': {
										'skipAd': {
											"enabled": true,
											"showAfterSeconds": 5
										 },
										'schedule': [
											{
												'position': 'pre-roll',
												'tag': 'http://delivery.adnetwork.vn/247/xmlvideoad/zid_1337054914/wid_1326251051/type_inline/cb_[timestamp]'
											},
											{
												'position': 'post-roll',
												'tag': 'http://delivery.adnetwork.vn/247/xmlvideoad/zid_1408004861/wid_1326251051/type_inline/cb_[timestamp]'
											}
										]
									}
								},
								'related-1': {
									'file': 'http://' + location.host + location.pathname +'index.related.xml',
									'onclick': '_target',
									'usedock': false,
									'dimensions': '160x90',
									'heading': 'Các video khác'
								}
							}
						});
					}
			}


			applyObject(videoConfs, confs);

			if (location.href.indexOf('/clip/') >= 0) {
					confs.autostart = true;
				}else {
					confs.autostart = false;
			}

			if (location.href.indexOf('/phim/phim') < 0) {
				jwplayer(confs.id).setup(videoConfs);

				/*if ($('#TVC_ADMICRO').is('div')) {
					var a = document.createElement("script");
					a.type = "text/javascript";
					a.async = true;
					a.src = 'http://ads.hosting.vcmedia.vn/js.ashx';
					var c = document.getElementsByTagName("script")[0];
					c.parentNode.insertBefore(a, c);
				}

				$('#quality-title').show();*/
			}

		break;
	}
};

function loadFile(file)
{
	if (jwplayer().renderingMode == 'html5') {
		confs = {file: videoReader + '?file=' + file}
	}else {
		confs = {file: file, streamer: videoReader}
	}

	jwplayer().load(confs);
}

function applyObject(target, source) {
	for (var key in source) {
		if (source.hasOwnProperty(key)) {
			target[key] = source[key];
		}
	}
};


function setLogPhim (data, obj) {
	$.ajax({
		url: 'http://truelife.vn/offica/movie/action',
		dataType: 'jsonp',
		type: 'GET',
		jsonp:"callback",
		data: {
			_f: 5555,
			server: data.server,
			file_url: data.file,
			io: data.io,
			gc_id: 1532,
			time: obj.time,
			token: obj.token,
			referrer: document.referrer,
			userAgent: navigator.userAgent
		},
		success: function(response){}
	});
}

function getToken(data) {
	$.ajax({
		url: 'http://truelife.vn/offica/resourcesubcription/action',
		dataType: 'jsonp',
		type: 'GET',
		jsonp:"callback",
		data: {
			_f: 6666,
			source: data.file,
			quality: $('#quality a.active').html(),
			type: 'movie'
		},
		success: function(response){
			if (response.success && response.object.length > 0) {
				setInterval(function () {
					setLogPhim(data, response.object[0]);
				}, 360000)
			}
		}
	});
}

function testServerCDN(data) {
	var d = {
		_f: 1,
		server: data.server,
		source: data.file,
		errorGc: 'Tạp trí MegaFun',
		gcId: 1532,
		type: 'Movie',
		q: $('#quality a.active').html(),
		userAgent: navigator.userAgent
	};

	$.ajax({
		url: 'http://'+data.server+'/asp.vs?callback=resultTestServerCDN',
		dataType: 'jsonp',
		type: 'GET',
		jsonp:"callback",
		success: function(response){
			if (response[0].status != 'OK') {
				setErrorCDN(d);
			}
		},
		error: function(response) {
			setErrorCDN(data);
		}
	});
}

function setErrorCDN(data) {
	$.ajax({
		url: 'http://truelife.vn/offica/cdn/serverstatus/action',
		dataType: 'jsonp',
		type: 'GET',
		jsonp:"callback",
		data: data,
		success: function(response){
			//Thành công
		},
		error: function() {
			//Error
		}
	});
}

function loadXMLDoc(dname) {
	if (window.XMLHttpRequest) {
		xhttp=new XMLHttpRequest();
	}
	else {
		xhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xhttp.open("GET",dname,false);
	xhttp.send();
	return xhttp.responseXML;
}

function countDVD() {
	$.ajax({
		url: 'http://stats.truelife.vn/offica/statistic/action',
		dataType: 'jsonp',
		type: 'GET',
		jsonp:"callback",
		data: {
			_f: 2,
			c: 0,
			s: 1532,
			u: 1532,
			m: 2,
			p: 'y4Uv3ncD/RReO|s3OlHqGao9ItfC6lse4QCxNfxvFdXOiQjuph89UEsZJJEqaPAB',
			t: 'MegaFun DVD ' + startTime()
		},
		success: function(response){},
		error: function() {}
	});
}
function startTime() {
	try {
		var today=new Date();
		var day=today.getUTCDate();
		var month=today.getUTCMonth() + 1;
		var h=today.getHours();
		var year=today.getFullYear();
		var m=today.getMinutes();
		var s=today.getSeconds();
		var stt = today.getDay()
		var weekday = ["Chủ nhật","Thứ 2","Thứ 3","Thứ 4","Thứ 5","Thứ 6", "Thứ 7"];

		// add a zero in front of numbers<10
		m=checkTime(m);
		s=checkTime(s);
		var b = (h < 12)?'Sáng':'Chiều';

		return(day + '/' + month + '/' + year);
	}
	catch (e) {}
}

function checkTime(i) {
	if (i<10)
	{
		i="0" + i;
	}
	return i;
}

