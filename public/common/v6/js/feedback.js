var getFeedUrl = "http://vnmedia.vn/service";
var sendFeedUrl = "http://vnmedia.vn/service";

$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

var feedback = function(config) {
	var self = this;

	this.start = function() {

		this.showfeedback(0);
		$(".feedback_form").submit(function( event ) {
			self.sendFeedback($(this));
			event.preventDefault();
		});

		$( "#fb-limit" ).change(function() {
			self.showfeedback(0);
		});
		$( "#fb-sort" ).change(function() {
			self.showfeedback(0);
		});
	}


	this.showfeedback = function(start) {
		$('.feedbacklist').html('');
		var article = $(".feedback_form")[0];
		var form_temp = '';
		form_temp = $('.feedback_form').clone();
		form_temp.submit(function( event ) {
			self.sendFeedback($(this));
			event.preventDefault();
		});
		$.ajax ({
			url: getFeedUrl + '/feedback/action',
			data: {
				_f: 26,
				articleId: article.articleId.value,
				communityId: article.communityId.value,
				s: 0,
				start: start*($('#fb-limit').val()),
				limit: $('#fb-limit').val(),
				filter: $('#fb-sort').val()
			},
			dataType: 'jsonp',
			jsonp: 'jsoncallback',
			context: this,
			success: function(response) {
				var strHtml = '';
				var obj = response.object;
				if(response.success && obj.length > 0) {
					$('.setting-holder').show();
					$.each(obj, function() {
						$.each(this, function() {
							if (this.id) {
								self.itemShow(this, form_temp);
							} else {
								$.each(this, function() {
									self.itemShow(this, form_temp);
								});
							}
						});
					});

					$('.pagging-list').smartpaginator({ totalrecords: response.header.rowsL1, recordsperpage: parseInt($('#fb-limit').val()), length: 5, next: '>>', prev: "<<", theme: 'red', controlsalways: true,
						initval: (start+1),
						onchange: function (newPage) {
							self.showfeedback(newPage-1);
						}
					});
				} else {
				}

			},
			error: function(response) {
			}
		});
	}

	this.itemShow = function (obj, form_temp){
		var self = this;
		var strReply = '';
		if(obj.level < 3) {
			strReply = '<div style="text-align: right"><a id="item_' + obj.id + '">Trả lời</a></div>';
		}
		strHtml = 
				'<li class="fb-li level-' + obj.level + '" id="' + obj.id + '">'+
					'<div class="fb-border">'+
						'<div class="">'+
							'<div class="fb-name">' + obj.displayName + '</div>'+
							'<div class="fb-time">' + self.prettyDate(obj.createDate) + '</div>'+
							'<div class="clear"></div>'+
							'<div class="fb-content">' + obj.content + '</div>'+
						'</div>'+
						'<div class="fb-button">'+
							'<div class="fb-answer" style="display: block;">'+
								strReply +
								'<div class="fb-form-item">'+
									'<div class="form_reply"></div>'+
									'<div class="fb-notification" style="display: none"></div>'+
									'<div class="clear"></div>'+
								'</div>'+
							'</div>'+
						'</div>'+
						'<div class="clear"></div>'+
					'</div>'+
				'</li>';
		$('.feedbacklist').append(strHtml);
		$('#item_' + obj.id).click(function(){
			console.log(form_temp[0]);
			form_temp[0].level.value = obj.level + 1;
			form_temp[0].l1Id.value = obj.l1.id;
			if(obj.level ==2) {
				form_temp[0].l2Id.value = obj.l2.id;
			}
			form_temp_c = $('<div class="fb-form"></div>').append(form_temp);
			$(this).parent().parent().find('.form_reply').append(form_temp_c);
		})
	}

	this.sendFeedback = function(form) {

			var buttonSubmit = form.find('input[type="submit"]');
			form[0].articleId.value = form[0].articleId.value.replace('vnmedia-', '');
			form[0].articleUrl.value = window.location.origin + form[0].articleUrl.value;
			buttonSubmit.attr('disabled', 'true');
			buttonSubmit.val('Đang gửi ...');
			var data = form.serializeObject();
			$.ajax ({
				url: sendFeedUrl + '/feedback/action',
				data: $.extend({
					_f: 1
				}, data),
				dataType: 'jsonp',
				jsonp: 'jsoncallback',
				context: this,
				success: function(response) {
					if(response.success) {
						form.parent().parent().parent().find(".fb-notification").show().html('Phản hồi thành công.');
						form[0].comment.value = '';
					} else {

						form.parent().parent().parent().find(".fb-notification").show().html('Có lỗi trong quá trình gửi. Bạn vui lòng quay lại sau');
					}
					buttonSubmit.removeAttr('disabled');
					buttonSubmit.val('Gửi phản hồi');
				},
				timeout: 5000,
				error: function(response) {
					buttonSubmit.removeAttr('disabled');
					buttonSubmit.val('Gửi phản hồi');
					form.parent().parent().parent().find(".fb-notification").show().html('Có lỗi trong quá trình gửi. Bạn vui lòng quay lại sau');
				}
			});
	}

	this.prettyDate = function(dateString) {
		// old date string: 2012-10-24 15:47:04.694
		// new date string (ISO 8601): 2012-10-23T16:22:21+07:00

		if (typeof dateString == 'undefined') {
			return '';
		}

		dateString = dateString.replace(/(\d{4}-\d{2}-\d{2}) (\d{2}:\d{2}:\d{2}).\d{0,4}/gim, '$1T$2+07:00');

		var date = new Date(dateString);

		if (isNaN(date)) {
			try {
				var dateArray = /(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2})([+\-])(\d{2}):(\d{2})/.exec(dateString);

				if (dateArray[1]) {
					date = dateArray[1].split(/\D/);

					for (var i = 0, L = date.length; i < L; i++) {
						date[i]= parseInt(date[i], 10) || 0;
					}

					date[1] -= 1;
					date = new Date(Date.UTC.apply(Date, date));

					if(!date.getDate()) {
						date = NaN;
					}
					else if (dateArray[3]) {
						var tz = (parseInt(dateArray[3], 10) * 60);
						if (dateArray[4]) tz += parseInt(dateArray[4], 10);
						if (dateArray[2] == '+') tz *= -1;
						if (tz) date.setUTCMinutes(date.getUTCMinutes()+ tz);
					}
				}
			}
			catch (e) {
				date = NaN;
			}
		}

		if (isNaN(date)) {
			return ''; //dateString incorrect
		}

		var now = new Date();
		var diff = (now.getTime() - date.getTime()) / 1000;
		var day_diff = Math.floor(diff / 86400);

		if (isNaN(day_diff) || day_diff < 0 || day_diff >= 1) {
			return 'hh:mm dd/MM/yyyy'
				.replace(/hh/, date.getHours())
				.replace(/mm/, date.getMinutes())
				.replace(/dd/, date.getDate())
				.replace(/MM/, date.getMonth() + 1)
				.replace(/yyyy/, date.getFullYear())
			;
		}

		return day_diff == 0 && (
			diff < 60 && 'Vừa gửi' ||
			diff < 120 && '1 phút trước' ||
			diff < 3600 && Math.floor( diff / 60 ) + ' phút trước' ||
			diff < 7200 && '1 giờ trước' ||
			diff < 86400 && Math.floor( diff / 3600 ) + ' giờ trước');
	}


}

feedback = new feedback({
});
feedback.start();

