/****************************************************************/
/* BEGIN of VnnAdsLoader Class by Thanh Bon, 01.2007, VASC                    */
/* http://javascriptcompressor.com/														   */
/****************************************************************/

function VnnAdsLoader(placeHolderId, data, brownInterval)
{
	this.error = '';

	if (document.getElementById(placeHolderId))
	{
		var boxControl = document.getElementById("box_" + placeHolderId);
		if (boxControl) {
			boxControl.style.display = "";
		}

		this.placeHolderControl = document.getElementById(placeHolderId);
		this.placeHolderControl.style.display = 'block';

		this.blocks = data;
		this.adCount = this.blocks.length;
		this.randPrefix = Math.floor(Math.random()*100000) + 100000;
		this.mode = this.placeHolderControl.getAttribute("adMode");
		this.direction = this.placeHolderControl.getAttribute("adDirection");
		this.width = this.placeHolderControl.getAttribute("adWidth");
		this.height = this.placeHolderControl.getAttribute("adHeight");
		this.classe = this.placeHolderControl.getAttribute("adClass");
		this.showTitle = this.placeHolderControl.getAttribute("adShowTitle");

		this.brownTimmer = 0;
		this.downloadCompleted = false;

		if (brownInterval) this.brownInterval = brownInterval;
		else this.brownInterval = 10000;

		// Dat gia tri vao block de su dung trong cac timmer
		for (var i=0; i<this.blocks.length; i++)
		{
			this.blocks[i].width = this.width;
			this.blocks[i].height = this.height;
			this.blocks[i].classe = this.classe;
			this.blocks[i].direction = this.direction;
		}
	}
	else	this.error = 'PLACE_HOLDER_NOT_FOUND';

	// methodes
	this.brownRunningZone = brownRunningZone;
	this.brownZone = brownZone;

	this.doLoad = doLoad;
	this.fadeBlock = fadeBlock;
	this.swapBlocks = swapBlocks;
	this.genSquelette = genSquelette;

	this.rotateBlock = rotateBlock;
	this.onRotateBlock = onRotateBlock;
	this.onRotateZone = onRotateZone;
	this.fillAdPlace = fillAdPlace;
	this.downloadImage = downloadImage;
	this.swapfade = swapfade;
	this.brownBlock = brownBlock;
	this.stopBlocksSwapping = stopBlocksSwapping;
	this.genObjectCode = genObjectCode;

	// animation methods
	this.JSFX_KeepInView = JSFX_KeepInView;
}

/*****************************************************************************
 List the images that need to be cached
*****************************************************************************/
function downloadImage()
{
	//cache the images
	this.blocks.cache = [];
	var imageIndex = 0;
	for(var i=0; i<this.adCount; i++)
	{
		this.blocks.cache[imageIndex] = new Image;
		this.blocks.cache[imageIndex].src = this.blocks[i].items[0].logo;
		imageIndex++;

		// write img tag
		var control_id = this.randPrefix + '_' + i;

		document.getElementById(control_id).style.display = 'block';
		this.fillAdPlace(control_id, this.blocks[i]);
	}

	for(var i=0; i<this.adCount; i++)
	{
		for (var j=1; j<this.blocks[i].items.length; j++)
		{
			this.blocks.cache[imageIndex] = new Image;
			this.blocks.cache[imageIndex].src = this.blocks[i].items[j].logo;
			imageIndex++;
		}
	}
	this.downloadCompleted = true;
}

function doLoad()
{
	if (this.error)	return;

	var self = this;

	this.placeHolderControl.innerHTML = this.genSquelette();

	this.downloadImage();

	// RANDOM FIRST IMAGE WHEN REFRESH PAGE
	if (self.mode == 'brown') {
		this.brownRunningZone(); // ramdom zone
	}
	//this.brownZone(); // random block's ads

	if (self.mode == 'brown' && this.brownInterval > 1)
	{
		window.clearInterval(self.brownTimmer);
		self.brownTimmer = window.setInterval(function() { self.brownRunningZone(); }, this.brownInterval);
	}

	this.onRotateZone();

}
/* Keep In View (c) JavaScript-FX. (www.javascript-fx.com)          */
function JSFX_KeepInView(id){
	var getPageY=function(el){return(el==null)?0:el.offsetTop+getPageY(el.offsetParent);};
	var getScrollTop=function(){return document.body.scrollTop||document.documentElement.scrollTop};
	var el=document.getElementById(id);if(el==null)return;
	if(el.style.position=="absolute"){el.startPageTop=-el.offsetTop;el.currentX=el.offsetLeft;el.currentY=el.offsetTop;}
	else{el.style.position="relative";el.startPageTop=getPageY(el);el.currentX=el.currentY=0;};
	el.floatInView=function(){
		var targetY=(getScrollTop()>this.startPageTop)?getScrollTop()-this.startPageTop:0;
		this.currentY+=(targetY-this.currentY)/4;this.style.top=this.currentY+"px";};
	setInterval('document.getElementById("'+id+'").floatInView()',40);
};

function onRotateZone()
{
	for (var i=0; i<this.blocks.length; i++)
	{
		this.onRotateBlock(this.blocks[i], i);
	}
}

function onRotateBlock(block, block_index)
{
	if ((block.rotateInterval > 0) && (block.items.length > 1))
	{

		block.img_control_id = '' + this.randPrefix + '_' + block_index;
		window.clearInterval(block.timmer);

		block.timmer = window.setInterval(function() {
			$('#' + block.img_control_id).children().hide();
			var randItem = Math.floor(Math.random()*block.items.length);
console.log($('#' + block.img_control_id).children().eq(randItem));
			$('#' + block.img_control_id).children().eq(randItem).show();
		}, block.rotateInterval);
	}
}

function stopBlocksSwapping()
{
	for (var i=0; i<this.blocks.length; i++)
		window.clearInterval(this.blocks[i].timmer);
}

// brown blocks when they are running
function brownRunningZone()
{
	this.stopBlocksSwapping();
	for (var i=0; i<this.blocks.length-1; i++)
	{

			if (!this.blocks[i].dock || this.blocks[i].dock == false)
			{
				//var rand = Math.floor(Math.random()*1000) % (this.blocks.length - i - 1) + i + 1;
				var rand = Math.floor(Math.random()*this.blocks.length);
				if (!this.blocks[rand].dock || this.blocks[rand].dock == false)
					this.swapBlocks(i, rand);
			}
	}
	this.onRotateZone();
}

// swap two blocks when they are running
function swapBlocks(index1, index2)
{
	if (this.downloadCompleted == false) return;

	var temp = this.blocks[index1];
	this.blocks[index1] = this.blocks[index2];
	this.blocks[index2] = temp;
	this.fadeBlock(this.blocks[index1], this.randPrefix + '_' + index1);
	this.fadeBlock(this.blocks[index2], this.randPrefix + '_' + index2);
}

function brownZone()
{

	// this.stopBlocksSwapping(); khong can goi ham nay

	for (var i=0; i<this.blocks.length; i++)
	{
		//this.brownBlock(this.blocks[i]);
		this.brownBlock(i);
	}
	//this.onRotateZone(); Neu bat dong thu nhat thi phai bat dong nay
}


function brownBlock(i)
{
;
	if (this.blocks[i].items.length < 2) return;

	var rand = Math.floor(Math.random()*(this.blocks[i].items.length-1)) + 1;

	var temp = this.blocks[i].items[0];

	swapBlocks(0, rand);
}

function fadeBlock(adArray, img_control_id)
{
	if (this.downloadCompleted == false) return;

	adArray.clock = null;
	adArray.fade = true;
	adArray.count = 1;

	var imgControl = document.getElementById(img_control_id);
	this.rotateBlock(adArray);
	this.swapfade(img_control_id, adArray, '1', '');
}

function rotateBlock(adArray)
{
	if (adArray.rotateInterval <=0) return;

	var len = adArray.items.length;

	if (len<=1) return;

	var item0 = adArray.items[0];

	for (var i=0; i<len-1; i++ )
	{
		adArray.items[i] = adArray.items[i+1];
	}

	adArray.items[len-1] = item0;
}

function genSquelette()
{


	var squelette = '<table border="0" cellspacing="0" cellpadding="0" width="100%">';


	for (var i=0; i<this.adCount; i++)
	{
		var linkId = this.randPrefix + '_' + i;

		// preview
		var placeHolder = '<table id="tbl_{temp_table_id}" style="background-color: #dedede; border: 3px solid #eee;" cellspacing="0" cellpadding="0" width="{width}px" height="{height}px"><tr><td align="center"><a href="{ad_url}" id="{ad_link_id}">{ad_title}</a></td></tr></table>';
		placeHolder = placeHolder.replace("{ad_title}", this.blocks[i].items[0].title);
		placeHolder = placeHolder.replace("{temp_table_id}", linkId);
		placeHolder = placeHolder.replace("{width}", (this.width==null)?this.blocks[i].items[0].width:this.width);
		placeHolder = placeHolder.replace("{height}", (this.height==null)?this.blocks[i].items[0].height:this.height);
		placeHolder = placeHolder.replace("{ad_url}", this.blocks[i].items[0].link);

		var adLink = '<tr><td align="center" valign="middle" id="{ad_link_id}">{ad_place_holder}</td></tr>';
		adLink = adLink.replace("{ad_link_id}", linkId);
		adLink = adLink.replace("{ad_place_holder}", placeHolder);
		squelette += adLink;
	}
	squelette +=	'</table>';
	return squelette;


}


function genObjectCode(control_id, block, isFirst)
{
	var rand = 0;
	if (isFirst) {
		if (block.items.length > 1) {
			rand = Math.floor(Math.random()*block.items.length);
			isFirst = false;
		}
	}
	var adHtml = '';
	var itemDisplay = "display:none";

	for (var i = 0; i < block.items.length ;i++ ){

		var type = block.items[i].type;
		if(rand == i) {
			 itemDisplay = "display:block;";
		} else {
			 itemDisplay = "display:none;";
		}
 

		if (typeof  type != 'undefined'  && type=='swf')
		{
			adHtml += '<div style="' + itemDisplay + '" onMousedown="adClick(\'{ad_stats_url}\', \'#\');"><embed src="{ad_src}" pluginspage="http://www.macromedia.com/go/getflashplayer" wmode="transparent" type="application/x-shockwave-flash" {ad_width} {ad_height}></div>';
		}
		else if (typeof type != 'undefined'  && (type=='script' || type=='html'))
		{
			adHtml += '<div style="' + itemDisplay + '">' + block.items[i].content + '</div>';
		}
		else
			adHtml += '<a style="'+itemDisplay+'" href="javascript:void(1);" onclick="javascript:adClick(\'{ad_stats_url}\', \'{ad_url}\');"><img class="{ad_class}" alt="{ad_alt}" {ad_title} {ad_width} {ad_height} src="{ad_src}" /></a>';

		adHtml = adHtml.replace(/{ad_src}/g, block.items[i].logo);
		adHtml = adHtml.replace(/{ad_class}/g, block.classe);
		adHtml = adHtml.replace("{ad_url}", block.items[i].link);
		adHtml = adHtml.replace("{ad_stats_url}", (block.items[i].stats + '&m=1'));
		adHtml = adHtml.replace("{ad_alt}", block.items[i].title);

		if (this.showTitle && this.showTitle == 'on')
		{
			adHtml = adHtml.replace("{ad_title}", 'title="' + block.items[i].title + '"');
		}
		else
		{
			adHtml = adHtml.replace("{ad_title}", "");
		}

		if (block.direction == 'horizontal')
		{
			adHtml = adHtml.replace(/{ad_height}/g, 'height="'+((block.height==null)?block.items[i].height:block.height)+'"');
			adHtml = adHtml.replace(/{ad_width}/g, 'width="'+((block.width==null)?block.items[i].width:block.width)+'"');
		}
		else
		{
			adHtml = adHtml.replace(/{ad_width}/g, 'width="'+((block.width==null)?block.items[i].width:block.width)+'"');
			if (typeof  type == 'undefined' || type=='swf')
				adHtml = adHtml.replace(/{ad_height}/g, 'height="'+((block.height==null)?block.items[i].height:block.height)+'"');
			else
				adHtml = adHtml.replace(/{ad_height}/g, '');
		}

	}
		return adHtml;
}



// Support Script Item:: BEGIN
function appendChild(node, text) {
  if (null == node.canHaveChildren || node.canHaveChildren) {
    node.appendChild(document.createTextNode(text));
  } else {
    node.text = text;
  }
}

function showScript(code) {
  //document.writeln(code);
}
// Support Script Item:: END

function fillAdPlace(control_id, block)
{

	var tempTable = document.getElementById('tbl_' + control_id);
	tempTable.parentNode.removeChild(tempTable);

    // Support Script Item:: BEGIN
	var type = block.items[0].type;
	var code = block.items[0].content;
    if (typeof  type != 'undefined'  && type=='_script') {
        var script = document.createElement("script");
        script.setAttribute('type','text/javascript');
        appendChild(script, "showScript('"+code+"');");
        document.getElementById(control_id).innerHTML = code;
    }
    // Support Script Item:: END
    else {
        document.getElementById(control_id).innerHTML = this.genObjectCode(control_id, block, true);
    }
}

// DOM scripting by brothercake -- http://www.brothercake.com/
function swapfade(img_control_id, adArray, resolution, alt)
{
	if(typeof alt != 'undefined' && alt != '')
	{
		adArray.obj.alt = alt;
	}
	var control = document.getElementById(img_control_id);
    // Support Script Item:: BEGIN
	var type = adArray.items[0].type;
	var code = adArray.items[0].content;
    if (typeof  type != 'undefined'  && type=='_script') {
        var script = document.createElement("script");
        script.setAttribute('type','text/javascript');
        appendChild(script, "showScript('"+code+"');");
        control.appendChild(script);
    }
    // Support Script Item:: END
    else {
        control.innerHTML = this.genObjectCode(img_control_id, adArray, false);
    }
};


function adClick(stats_url, ad_url)
{
	var r = "http://sime.vn";

	if (document.referrer) r = escape(document.referrer);
	try {
		if (ad_url != '#') {
			newwin = window.open(ad_url, '_blank');
		}
		element = document.createElement('div');
		element.innerHTML = '<img id="stat_img" style="display: none;" alt="" src="' +stats_url.replace("{REFERRER}", r) + '" width="0" height="0"/>';
		document.body.appendChild(element);
	}
	catch (e) {}

	return false;
}
