

(function ($) {
    "use strict";
    $(document).ready(function () {
        $('.tabs_new_top').tabs();
        $('.scroll_hot_list').mCustomScrollbar({
            scrollButtons: {
                enable: false
            },
            autoHideScrollbar: false,
            theme: "3d-dark"
        });
        $('.scroll_other_list').mCustomScrollbar({
            scrollButtons: {
                enable: false
            },
            autoHideScrollbar: false,
            theme: "3d-dark"
        });
        $('.share_cmt').click(function () {
            $(this).closest('.block_like_web').find('.block_share_cmt_fb').slideToggle();
        });
        $('.reply_cmt').on('click', function () {
            $(this).closest('.width_comment').find('.block_input_comment').slideToggle();
            $(this).closest('.sub_comment').find('.block_input_comment').slideToggle();
        });
        /* Show Comment */
        $('.fillter_comment .comment_news').on('click', function () {
            $(this).closest('.block_show_comment').find('.show_comment_1').show();
            $(this).closest('.block_show_comment').find('.show_comment_2').hide();
            $(this).closest('.block_show_comment').find('.show_comment_3').hide();
            var a_this = $(this);
            if (!a_this.hasClass('active')) {
                a_this.parent('.fillter_comment').find('a').removeClass('active');
                a_this.addClass('active');
            }
        });
        $('.fillter_comment .comment_likew').on('click', function () {
            $(this).closest('.block_show_comment').find('.show_comment_2').show();
            $(this).closest('.block_show_comment').find('.show_comment_1').hide();
            $(this).closest('.block_show_comment').find('.show_comment_3').hide();
            var a_this = $(this);
            if (!a_this.hasClass('active')) {
                a_this.parent('.fillter_comment').find('a').removeClass('active');
                a_this.addClass('active');
            }
        });
        $('.fillter_comment .comment_all').on('click', function () {
            $(this).closest('.block_show_comment').find('.show_comment_3').show();
            $(this).closest('.block_show_comment').find('.show_comment_1').hide();
            $(this).closest('.block_show_comment').find('.show_comment_2').hide();
            var a_this = $(this);
            if (!a_this.hasClass('active')) {
                a_this.parent('.fillter_comment').find('a').removeClass('active');
                a_this.addClass('active');
            }
        });
        $('.box_search a').click(function () {
            $('.box_search .input_search').stop().slideToggle();
            $('.box_search .input_search input')[0].focus()
        });
        $('.navbar_mobile').click(function () {
            $('.menu_nav').stop().slideToggle();
        });
        $('div.cut_string').each(function(){
            var limit = parseInt($(this).attr('data-limit'));
            var text = $(this).text();
            if(text.length > limit) $(this).html(text.substring(0, limit) + '...<i class="fa fa-angle-down"></i>');
        });
        $('div.cut_string').on('click', 'i', function(){
            var div = $(this).parent('div.cut_string');
            if($(this).hasClass('fa-angle-down')) div.html(div.next().text() + '<i class="fa fa-angle-up"></i>');
            else div.html(div.next().text().substring(0, div.attr('data-limit')) + '...<i class="fa fa-angle-down"></i>');
        });
        // hiện QC mobi
        if ($(window).width() < 800) {
            $('#ArticlesBottomMOBI').removeClass('box-ad-MOBI');
        }

    });
})(jQuery);
