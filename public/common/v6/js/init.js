// $(document).ready(init);
// function init(){
//
// }

// // Module Th?i ti?t
// var InfoLib = {
//     url_ServiceApi: "https://service.baomoi.com/"
// };
// InfoLib.FormattedDate = function(e, t) {
//     var n = e.getFullYear()
//       , i = (1 + e.getMonth()).toString();
//     i = i.length > 1 ? i : "0" + i;
//     var o = e.getDate().toString();
//     return o = o.length > 1 ? o : "0" + o,
//     t ? o + "/" + i + "/" + n : o + "/" + i
// }
// ,
// InfoLib.ViewWeather = function() {
//     $.get(InfoLib.url_ServiceApi + "weatherlist.json", function(e) {
//         var t = $("#weather__today");
//         null != t && void 0 != t && t.length > 0 && t.remove(),
//         $(".weather").append('<div id="weather__today" class="weather__today"></div>'),
//         t = $("#weather__today"),
//         t.append('<select id="weather-location" class="weather__location" onchange="InfoLib.DataWeather()"></select>'),
//         objWeatherSelect = $("#weather-location");
//         for (var n in e)
// 			if(e[n].ShortName == 'hanoi') {
// 				objWeatherSelect.append("<option selected value='" + e[n].ShortName + "'>" + e[n].CityName + "</option>");
// 			} else {
// 				objWeatherSelect.append("<option value='" + e[n].ShortName + "'>" + e[n].CityName + "</option>");
// 			}
//         InfoLib.DataWeather()
//     })
// }
// ,
// InfoLib.DataWeather = function() {
//     var e = $("#weather__info");
//     null != e && void 0 != e && e.length > 0 && e.remove();
//     var t = $("#weather__listing");
//     null != t && void 0 != t && t.length > 0 && t.remove();
//     var n = $("#weather-location").val();
//     n + "" != "" && void 0 != n && $.get(InfoLib.url_ServiceApi + "weather-" + n + "-4.json", function(n) {
//         if (e = $("#weather__info"),
//         null != e && void 0 != e && e.length > 0 && e.remove(),
//         t = $("#weather__listing"),
//         null != t && void 0 != t && t.length > 0 && t.remove(),
//         null != n && void 0 != n && n.length > 0) {
//             $("#weather__today").append('<div id="weather__info" class="weather__info"></div>'),
//             e = $("#weather__info"),
//             e.append('<i class="spr spr--w-' + n[0].CA + '"></i>'),
//             e.append('<span class="dc">' + n[0].CT + "&deg;</span>"),
//             e.append('<span class="temp">' + n[0].MinT + "&deg; - " + n[0].MaxT + "&deg;</span>"),
//             e.append('<span class="info">' + n[0].CI + "</span>"),
//             $(".weather").append('<ul id="weather__listing" class="weather__listing"></ul>'),
//             t = $("#weather__listing");
//             var i = new Date;
//             i.setDate(i.getDate() + 1),
//             t.append('<li class="is-first"><p class="day">' + InfoLib.FormattedDate(i, !1) + '</p><p class="icon"><i class="spr spr--w-' + n[0].A1 + '-mini"></i></p><p class="temp">' + n[0].MinT1 + "&deg; - " + n[0].MaxT1 + "&deg;</p></li>"),
//             i.setDate(i.getDate() + 1),
//             t.append('<li><p class="day">' + InfoLib.FormattedDate(i, !1) + '</p><p class="icon"><i class="spr spr--w-' + n[0].A2 + '-mini"></i></p><p class="temp">' + n[0].MinT2 + "&deg; - " + n[0].MaxT2 + "&deg;</p></li>"),
//             i.setDate(i.getDate() + 1),
//             t.append('<li><p class="day">' + InfoLib.FormattedDate(i, !1) + '</p><p class="icon"><i class="spr spr--w-' + n[0].A3 + '-mini"></i></p><p class="temp">' + n[0].MinT3 + "&deg; - " + n[0].MaxT3 + "&deg;</p></li>"),
//             i.setDate(i.getDate() + 1),
//             t.append('<li><p class="day">' + InfoLib.FormattedDate(i, !1) + '</p><p class="icon"><i class="spr spr--w-' + n[0].A4 + '-mini"></i></p><p class="temp">' + n[0].MinT4 + "&deg; - " + n[0].MaxT4 + "&deg;</p></li>")
//         }
//     })
// }
// ,
// InfoLib.WeatherInfo = function() {
//     $.get(InfoLib.url_ServiceApi + "weatherlist.json", function(e) {
//         var t = $(".panel-weather");
//         if (null != t && void 0 != t && t.length > 0) {
//             var n = $(".weather");
//             null != n && void 0 != n && n.length > 0 && n.remove(),
//             t.append('<div class="weather"></div>'),
//             n = $(".weather"),
//             n.append('<div class="weather__today"></div>');
//             var i = $(".weather__today");
//             i.append('<select id="weather__location" class="weather__location" onchange="InfoLib.WeatherInLocation()"></select>');
//             var o = $("#weather__location");
//             for (var r in e)
//                 o.append("<option value='" + e[r].ShortName + "'>" + e[r].CityName + "</option>");
//             InfoLib.WeatherInLocation()
//         }
//     })
// }
// ,
// InfoLib.WeatherInLocation = function() {
//     var e = $("#weather__location").val();
//     if (e + "" != "" && void 0 != e)
//         $.get(InfoLib.url_ServiceApi + "weather-" + e + "-7.json", function(e) {
//             var t = $(".weather__info");
//             null != t && void 0 != t && t.length > 0 && t.remove();
//             var n = $(".weather__listing");
//             if (null != n && void 0 != n && n.length > 0 && n.remove(),
//             e.length > 0) {
//                 var i = $(".weather__today");
//                 i.append('<div class="weather__info"></div>');
//                 var t = $(".weather__info");
//                 t.append('<i class="spr spr--w-' + e[0].CA + '"></i>'),
//                 t.append('<span class="dc">' + e[0].CT + "&deg;</span>"),
//                 t.append('<span class="temp">' + e[0].MinT + "&deg; - " + e[0].MaxT + "&deg;</span>"),
//                 t.append('<span class="info">' + e[0].I + "</span>");
//                 var o = $(".weather");
//                 o.append('<ul class="weather__listing"></ul>');
//                 var n = $(".weather__listing")
//                   , r = new Date;
//                 r.setDate(r.getDate() + 1),
//                 n.append('<li class="is-first"><p class="day">' + InfoLib.FormattedDate(r, !1) + '</p><p class="icon"><i class="spr spr--w-' + e[0].A1 + '-mini"></i></p><p class="temp">' + e[0].MinT1 + "&deg; - " + e[0].MaxT1 + "&deg;</p></li>"),
//                 r.setDate(r.getDate() + 1),
//                 n.append('<li><p class="day">' + InfoLib.FormattedDate(r, !1) + '</p><p class="icon"><i class="spr spr--w-' + e[0].A2 + '-mini"></i></p><p class="temp">' + e[0].MinT2 + "&deg; - " + e[0].MaxT2 + "&deg;</p></li>"),
//                 r.setDate(r.getDate() + 1),
//                 n.append('<li><p class="day">' + InfoLib.FormattedDate(r, !1) + '</p><p class="icon"><i class="spr spr--w-' + e[0].A3 + '-mini"></i></p><p class="temp">' + e[0].MinT3 + "&deg; - " + e[0].MaxT3 + "&deg;</p></li>"),
//                 r.setDate(r.getDate() + 1),
//                 n.append('<li><p class="day">' + InfoLib.FormattedDate(r, !1) + '</p><p class="icon"><i class="spr spr--w-' + e[0].A4 + '-mini"></i></p><p class="temp">' + e[0].MinT4 + "&deg; - " + e[0].MaxT4 + "&deg;</p></li>"),
//                 r.setDate(r.getDate() + 1),
//                 n.append('<li><p class="day">' + InfoLib.FormattedDate(r, !1) + '</p><p class="icon"><i class="spr spr--w-' + e[0].A5 + '-mini"></i></p><p class="temp">' + e[0].MinT5 + "&deg; - " + e[0].MaxT5 + "&deg;</p></li>"),
//                 r.setDate(r.getDate() + 1),
//                 n.append('<li><p class="day">' + InfoLib.FormattedDate(r, !1) + '</p><p class="icon"><i class="spr spr--w-' + e[0].A6 + '-mini"></i></p><p class="temp">' + e[0].MinT6 + "&deg; - " + e[0].MaxT6 + "&deg;</p></li>"),
//                 r.setDate(r.getDate() + 1),
//                 n.append('<li><p class="day">' + InfoLib.FormattedDate(r, !1) + '</p><p class="icon"><i class="spr spr--w-' + e[0].A7 + '-mini"></i></p><p class="temp">' + e[0].MinT7 + "&deg; - " + e[0].MaxT7 + "&deg;</p></li>")
//             }
//         });
//     else {
//         var t = $(".weather__info");
//         null != t && void 0 != t && t.length > 0 && t.remove();
//         var n = $(".weather__listing");
//         null != n && void 0 != n && n.length > 0 && n.remove()
//     }
// }
// ,
// InfoLib.Load = function() {
//     InfoLib.ViewWeather()
//     InfoLib.WeatherInfo()
// }
// ,
// InfoLib.Load();
