<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('company');

        Schema::create('company', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('province_id');
            $table->integer('district_id')->nullable();
            $table->integer('ward_id')->nullable();
            $table->integer('company_size_id');
            $table->string('name');
            $table->string('slug');
            $table->string('status');
            $table->string('logo');
            $table->string('phone')->nullable();
            $table->string('website')->nullable();
            $table->mediumText('address')->nullable();
            $table->mediumText('description')->nullable();
            $table->string('contact_name')->nullable();
            $table->string('contact_email')->nullable();
            $table->string('contact_phone')->nullable();
            $table->integer('job_total_count')->default(0);
            $table->integer('job_avail_count')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company', function (Blueprint $table) {
            //
        });
    }
}
