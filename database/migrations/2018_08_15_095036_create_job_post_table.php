<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobPostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_post', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('video_id');
            $table->integer('company_id');
            $table->string('job_title');
            $table->string('slug');
            $table->string('thumb')->nullable();
            $table->string('job_description');
            $table->string('job_requirement');
            $table->string('	job_benefit');
            $table->string('	job_cv');

            $table->integer('job_level_id');
            $table->integer('job_type_id');
            $table->integer('job_salary_id');
            $table->integer('job_position_id');
            $table->integer('job_experience_id');
            $table->integer('gender');
            $table->integer('number_of_recruitment');
            $table->string('	note')->nullable();
            $table->integer('is_featured')->nullable();
            $table->string('	status');
            $table->timestamp('ended_at')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_post');
    }
}
