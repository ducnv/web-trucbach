<?php

use Illuminate\Database\Seeder;

use App\Models\User;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    public function run(){
        DB::table('users')->insert([
            'name' => 'Admin',
            'username' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('secret'),
            'avatar' => '',
            'phone' => '',
            'role_id' => 1,
            'session_id' => '',
            'last_login' => Carbon::now(),
            'last_logout' => Carbon::now(),
            'last_login_ip' => '',
            'api_token' => str_random(100),
        ]);

        DB::table('users')->insert([
            'name' => 'ducnv',
            'username' => 'ducnv',
            'email' => 'ducnv@traithivang.vn',
            'password' => bcrypt('TraiThiVang'),
            'avatar' => '',
            'phone' => '',
            'role_id' => 1,
            'session_id' => '',
            'last_login' => Carbon::now(),
            'last_logout' => Carbon::now(),
            'last_login_ip' => '',
            'api_token' => str_random(100),
        ]);

        DB::table('users')->insert([
            'name' => 'hauhm',
            'username' => 'hauhm',
            'email' => 'hauhm@traithivang.vn',
            'password' => bcrypt('TraiThiVang'),
            'avatar' => '',
            'phone' => '',
            'role_id' => 1,
            'session_id' => '',
            'last_login' => Carbon::now(),
            'last_logout' => Carbon::now(),
            'last_login_ip' => '',
            'api_token' => str_random(100),
        ]);

        DB::table('users')->insert([
            'name' => 'trangdnt',
            'username' => 'trangdnt',
            'email' => 'trangdnt@traithivang.vn',
            'password' => bcrypt('TraiThiVang'),
            'avatar' => '',
            'phone' => '',
            'role_id' => 1,
            'session_id' => '',
            'last_login' => Carbon::now(),
            'last_logout' => Carbon::now(),
            'last_login_ip' => '',
            'api_token' => str_random(100),
        ]);
    }
}