<?php

use Illuminate\Database\Seeder;

use App\Models\Roles;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $super_admin = new Roles();
        $super_admin->name = 'superadmin';
        $super_admin->display_name = 'Quản trị viên';
        $super_admin->description = 'Quản trị viên';
        $super_admin->permissions = [];
        $super_admin->save();

        $mod = new Roles();
        $mod->name = 'content_manager';
        $mod->display_name = 'TBP Nội dung';
        $mod->description = '';
        $mod->permissions = [];
        $mod->save();

        $bizmod = new Roles();
        $bizmod->name = 'business_manager';
        $bizmod->display_name = 'TBP Kinh doanh';
        $bizmod->description = '';
        $bizmod->permissions = [];
        $bizmod->save();

        $content_user = new Roles();
        $content_user->name = 'content_editor';
        $content_user->display_name = 'Biên tập viên Nội dung';
        $content_user->description = '';
        $content_user->permissions = [];
        $content_user->save();

        $biz_user = new Roles();
        $biz_user->name = 'business_employee';
        $biz_user->display_name = 'Nhân viên Kinh doanh';
        $biz_user->description = '';
        $biz_user->permissions = [];
        $biz_user->save();

        $collaborators = new Roles();
        $collaborators->name = 'collaborators';
        $collaborators->display_name = 'Cộng tác viên';
        $collaborators->description = '';
        $collaborators->permissions = [];
        $collaborators->save();
    }
}
