<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $carrer = [
            "Lái xe" => "https://tuyencongnhan.vn/tim-viec/lai-xe",
        ];

        foreach ($carrer as $name => $url) {
            $site = new \App\Models\SiteCrawlerCategories();
            $site->name = $name;
            $site->slug = $url;
            $site->sitecrawler_id = 1;
            $site->method = "TuyenCongNhan";
            $site->save();
        }

    }
}
