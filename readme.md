## ThiVangCMS - CRAWLER

## Giới thiệu

## Lịch sử thay đổi

## Hướng dẫn cài đặt

- Thêm key SSH vào tài khoản gitlab.com

- Clone repository bằng lệnh : `git@gitlab.com:ttv-app-dev/cms-site-crawler.git`

- Chạy `composer install` để cài đặt các component cần thiết cho Laravel

- Chạy lệnh `npm install` để cài đặt các package cần thiết cho views, để chạy được yêu cầu cài nodejs (ví dụ v8.11.) 

- Chạy lệnh `npm run dev` để combine ra các file css,js cho views

- Sửa thông tin kết nối DB trong file `.env`. Sau đó chạy lệnh `php artisan migrate` để thực hiện tạo bảng

- Chạy php artisan db:seed để tạo dữ liệu mẫu

- Chạy php artisan serve để chạy project và đăng nhập với account admin@admin.com/secret

- Chạy lệnh `npm run watch` và bắt đầu coding

## Giấy phép

Trai Thi Vang Joint Stock Company - Copyright 2017