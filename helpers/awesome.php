<?php

if (!function_exists('parse_package_config')) {
    function parse_package_config($groupByKey)
    {
        $merged_arr = [];
        $config = collect(config('core'));
        foreach ($config as $item) {
            $merged_arr = array_merge($merged_arr, $item[$groupByKey]);
        }
        return collect($merged_arr)->sortBy('priority', SORT_REGULAR, true);
    }
}

if (!function_exists('checkHDH')) {
    function checkHDH()
    {
        //Detect special conditions devices
        $iPod = stripos($_SERVER['HTTP_USER_AGENT'], "iPod");
        $iPhone = stripos($_SERVER['HTTP_USER_AGENT'], "iPhone");
        $iPad = stripos($_SERVER['HTTP_USER_AGENT'], "iPad");
        $Android = stripos($_SERVER['HTTP_USER_AGENT'], "Android");
        $webOS = stripos($_SERVER['HTTP_USER_AGENT'], "webOS");

        if ($iPod || $iPhone || $iPad) {
            return 'ios';
        } else if ($Android) {
            return 'android';
        }else {
            return 'web';
        }
    }
}


if (!function_exists('getslug')) {
    function getslug($item)
    {
        return '/' . $item->category->slug . '/' . $item->slug . '.html';
    }
}

if (!function_exists('annotation_reader')) {
    function annotation_reader($class, $method)
    {
        $annotationReader = new Doctrine\Common\Annotations\AnnotationReader();
        $reflectionMethod = new ReflectionMethod($class, $method);
        $methodAnnotation = $annotationReader->getMethodAnnotations($reflectionMethod);

        foreach ($methodAnnotation as $access) {
            if (isset($access->permission)) {
                return $access->permission;
            }
        }
        return null;
    }
}

if (!function_exists('has_permission')) {
    function has_permission($user_id, $permissions)
    {
        return true;
        if ($user_id == 1) {
            return true;
        }

        $role_permissions = env('APP_DEBUG')
            ? App\Models\User::findOrFail($user_id)->role->permissions
            : Cache::remember('user_permission_' . $user_id, 60, function () use ($user_id) {
                return App\Models\User::findOrFail($user_id)->role->permissions;
            });

        $permissions = explode('|', $permissions);

        foreach ($permissions as $per) {
            if (in_array($per, $role_permissions)) {
                return true;
            }
        }

        return false;
    }
}

if (!function_exists('getVideoAttributes')) {
    function getVideoAttributes($filepath)
    {
        $command = 'ffmpeg -i ' . storage_path('app/public/' . $filepath) . ' -vstats 2>&1';

        $output = shell_exec($command);

        $regex_sizes = '/(\b[^0]\d+x[^0]\d+\b)/';

        try {
            preg_match($regex_sizes, $output, $regs);
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
        if (!empty($regs)) {
            list($width, $height) = explode('x', reset($regs));
        } else {
            $codec = '';
            $width = '';
            $height = '';
        }

        $regex_duration = '/Duration: ([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2}).([0-9]{1,2})/';
        if (preg_match($regex_duration, $output, $regs)) {
            $hours = $regs[1] ? $regs[1] : null;
            $mins = $regs[2] ? $regs[2] : null;
            $secs = $regs[3] ? $regs[3] : null;
            $ms = $regs[4] ? $regs[4] : null;
        }

        return [
            'width' => $width,
            'height' => $height,
            'hours' => $hours,
            'mins' => $mins,
            'secs' => $secs,
            'ms' => $ms,
        ];
    }
}

if (!function_exists('getformattime')) {

    function getformattime($format, $time)
    {
        return date($format, strtotime($time));
    }
}

if (!function_exists('convertmonthtoenglish')) {

    function convertmonthtoenglish($time)
    {
        $month = '';
        switch (intval(date("m", strtotime($time)))) {
            case 1:
                $month = "Jan";
                break;
            case 2:
                $month = "Feb";
                break;
            case 3:
                $month = "Mar";
                break;
            case 4:
                $month = "Apr";
                break;
            case 5:
                $month = "May";
                break;
            case 6:
                $month = "Jun";
                break;
            case 7:
                $month = "Jul";
                break;
            case 8:
                $month = "Aug";
                break;
            case 9:
                $month = "Sept";
                break;
            case 10:
                $month = "Oct";
                break;
            case 11:
                $month = "Nov";
                break;
            case 12:
                $month = "Dec";
                break;
        }

        return $month;
    }
}

if (!function_exists('getimageresize')) {

    function getimageresize($size, $url)
    {
        return str_replace(['.png', '.jpg', '.jpeg'], '-' . $size . '.png', $url);
    }
}

if (!function_exists('getconfig')) {

    function getconfig($key)
    {
        $conf = \App\Models\Setting::where('key', $key)->first();
        if ($conf) {
            return $conf->value;
        }
        return '';
    }
}


