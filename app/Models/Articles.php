<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Articles extends Model
{
    use SoftDeletes;

    protected $table = 'article';

    protected $fillable = [
        'title', 'description', 'body', 'thumb', 'category_id', 'slug'
    ];

    protected $casts = [

    ];

    protected $dates = ['deleted_at'];

    protected $appends = [

    ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function listTags()
    {
        return $this->belongsToMany(Tags::class, 'article_tag', 'article_id', 'tag_id');
    }


    public function hit()
    {
        return $this->updateStats();
    }

    public function updateStats($date = null)
    {
        //checks for a valid date format
        if (!empty($date) && !self::validDateFormat($date)) {
            return false;
        }
        $date = empty($date) ? gmdate('Y-m-d') : $date;

        $raw_stats = convertjsondecode($this->raw_stats);

        // Update model with new stats
        $this->one_day_stats = $this->_calculate_days_stats(1, $raw_stats, $date);
        $this->seven_days_stats = $this->_calculate_days_stats(7, $raw_stats, $date);
        $this->thirty_days_stats = $this->_calculate_days_stats(30, $raw_stats, $date);
        $this->all_time_stats = $this->all_time_stats + 1;

        // Update raw_stats for date
        if (isset($raw_stats) && count($raw_stats) >= 30) {
            // remove older than 30 days stats
            array_shift($raw_stats);
            // add new date
            $raw_stats[$date] = 1;
        } else {
            if (!isset($raw_stats[$date])) {
                //ducnv them vao
//                $raw_stats = array();
                if (isset($raw_stats[$date])) {
                    $raw_stats[$date] = 1;
                } else {
                    $raw_stats = array();
                    $raw_stats[$date] = 1;
                }
            } else {
                $raw_stats[$date] = $raw_stats[$date] + 1;
            }
        }

        $this->raw_stats = json_encode($raw_stats);

        return $this->save();
    }

    private function validDateFormat($date)
    {
        $result = \DateTime::createFromFormat('Y-m-d', $date) !== FALSE;
        return $result;
    }

    private function _calculate_days_stats($days, $existing_stats, $date)
    {
        if ($existing_stats && $days == 1) {
            if (isset($existing_stats[$date])) {
                return (int)$existing_stats[$date] + 1;
            }
        } else if ($existing_stats) {
            $extra_to_add = 0;
            if (isset($existing_stats[$date])) {
                $extra_to_add = (int)$existing_stats[$date];
            }
            $total = 0;
            for ($i = 1; $i < $days; $i++) {
                $timestampDate = strtotime($date);
                // calculate relative date to provided $date
                $old_date = date('Y-m-d', strtotime("-{$i} days", $timestampDate));
                if (isset($existing_stats[$old_date])) {
                    $total += (int)$existing_stats[$old_date];
                }
            }
            return $total + $extra_to_add + 1;
        }
        return 1;
    }

}