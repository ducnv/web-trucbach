<?php

namespace App\Models;

use Cache;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $table = 'media_storage';

    protected $fillable = [
        'user_id',
        'category_id',
        'media_type',
        'uuid',
        'name',
        'alt',
        'caption',
        'link',
        'thumb',
        'file_path',
        'extension',
        'height',
        'width',
        'length',
        'version',
        'mime',
        'file_name',
        'file_url'
    ];

    protected $casts = [
        'version' => 'array'
    ];

    protected $appends = [

    ];

}
