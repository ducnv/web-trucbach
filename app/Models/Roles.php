<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $table = 'roles';

    protected $fillable = [
    ];

    protected $casts = [
        'permissions' => 'array',
    ];

    public function user()
    {
        return $this->hasMany('App\Models\User', 'role_id');
    }


    public function getPermissionsAttribute($value)
    {
        return json_decode($value);
    }
}