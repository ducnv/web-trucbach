<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{

    protected $table = 'tags';


    public function article()
    {
        return $this->belongsToMany(Articles::class, 'article_tag', 'tag_id', 'article_id');
    }

}
