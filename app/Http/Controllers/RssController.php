<?php

namespace App\Http\Controllers;

use App\Models\Articles;
use App\Models\JobPost;
use App\Models\Pages;
use Illuminate\Http\Request;

class RssController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($sitemap)
    {
        return response()->view('sitemap.' . $sitemap)->header('Content-Type', 'application/rss+xml; charset=UTF-8');
    }

    public function post($date)
    {
        $news = Articles::where('created_at', '<=', $date . "-" . cal_days_in_month(CAL_GREGORIAN, explode("-", $date)[1], explode("-", $date)[0]) . " 23:59:59")
            ->where('created_at', '>=', $date . "-01 00:00:00")
            ->where('status', 'yes')
            ->orderBy('created_at', 'DESC')->get();
        return response()->view('sitemap.sitemap-post', compact("news"))->header('Content-Type', 'application/rss+xml; charset=UTF-8');

    }


    public function invoice($date)
    {
        $news = JobPost::where('created_at', '<=', $date . "-" . cal_days_in_month(CAL_GREGORIAN, explode("-", $date)[1], explode("-", $date)[0]) . " 23:59:59")
            ->where('created_at', '>=', $date . "-01 00:00:00")
            ->where('status', 'yes')
            ->orderBy('created_at', 'DESC')->get();
        return response()->view('sitemap.sitemap-invoice', compact("news"))->header('Content-Type', 'application/rss+xml; charset=UTF-8');

    }
}
