<?php

namespace App\Http\Controllers;

use App\Models\Articles;
use App\Models\Category;
use App\Models\JobPost;
use App\Models\Pages;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $msisdn = $request->header('msisdn') ? $request->header('msisdn') : false;

        $requestid = time();
        $returnurl = 'http://sanlocvang.com.vn/luat-choi';
        $backurl = 'http://sanlocvang.com.vn/luat-choi';
        $cpid = 1000684;
        $service_id = 1001035;
        $package_id = 1012485;
        $requestdatetime = date('YmdHms');

        $channel = 'WAP';
        $random = $this->random();
        $securecode = md5($random . "pre_register.jsp" . "pre_register.jsp" . ((string)$requestdatetime) . $channel . "vasgate@13579");
        $securecodeHuy = 'vasgate@13579';
        $h_sc = md5("vasgate@13579" . $random);

        $this->views['urlRg'] = 'http://bss.vascloud.com.vn/unify/register.jsp?requestid=' . $requestid . '&returnurl=' . $returnurl . '&backurl=' . $backurl . '&cp=' . $cpid . '&service=' . $service_id . '&package=' . $package_id . '&requestdatetime=' . $requestdatetime . '&channel=' . $channel . '&securecode=' . $securecode . '&h_sc=' . $h_sc . '';

        $this->views['urlHuy'] = 'http://bss.vascloud.com.vn/unify/cancel.jsp?requestid=' . $requestid . '&returnurl=' . $returnurl . '&backurl=' . $backurl . '&cp=' . $cpid . '&service=' . $service_id . '&package=' . $package_id . '&requestdatetime=' . $requestdatetime . ']&channel=' . $channel . '&securecode=' . $securecodeHuy . '';

        $this->views['msisdn'] = $msisdn;
        //lay ra 12 tin tuc
        $this->views['news'] = Articles::where('status', 'yes')->orderBy('created_at', 'DESC')->take(12)->get();

        $this->views['categories'] = Category::where('status', 'yes')->get();

        return view('home.index', $this->views);
    }

    public function test()
    {
        return 1;
    }
    public function random()
    {
        return (string)rand(1, 999999);
    }

}
