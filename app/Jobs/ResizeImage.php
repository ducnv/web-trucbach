<?php

namespace App\Jobs;

use Log;
use File;
use Image;
use Storage;
use App\SystemEvents;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ResizeImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $quality = 90;

    public $path;
    public $link;
    public $orientation;
    public $version = ['640x480', '640x360', '200x200'];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($path, $orientation)
    {
        $this->path = $path;
        $this->orientation = $orientation;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach($this->version as $ver){
            list($width, $height) = explode('x', $ver);
            $this->resizeImage($this->path, $width, $height, $this->orientation);
        }
    }

    private function resizeImage($path, $width, $height, $orientation)
    {
        try{
            $image = Image::make(storage_path('app/public/' . $path));
            
            // $image->resize($width, null, function ($constraint) {
            //     $constraint->aspectRatio();
            // });
            // if($image->height() >= $width){
            //     $image->crop($width, $height);
            // }else if(1 - $image->height() / $height <= 0.15){
            //     $image->resize($width, $height);
            // }
            // $image->save(storage_path('app/public/' . dirname($path) . '/' . $image->filename . '-' . $width . 'x' . $height . '.png'), $this->quality);
            // if ($orientation == 'portrait') {
                $image->resize($width, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
//                if($width === $height){
//                    $image->crop($width, $height);
//                }
                $image->save(storage_path('app/public/' . dirname($path) . '/' . $image->filename . '-' . $width . 'x' . $height . '.png'), $this->quality);
                //->crop($width, $height)->
            // } else {
            //     $image->resize(null, $width, function ($constraint) {
            //         $constraint->aspectRatio();
            //     })->resizeCanvas($width, $height)->save(storage_path('app/public/' . dirname($path) . '/' . $image->filename . '-' . $width . 'x' . $height . '.png'), $this->quality);
            // }
        }catch(\Exception $ex){

        }
    }
}
